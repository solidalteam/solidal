package it.albertodaleo.app.web.rest;

import com.google.common.collect.Sets;
import it.albertodaleo.app.SolidalApp;
import it.albertodaleo.app.builder.CompanyBuilder;
import it.albertodaleo.app.domain.Authority;
import it.albertodaleo.app.domain.Company;
import it.albertodaleo.app.domain.User;
import it.albertodaleo.app.repository.CompanyRepository;
import it.albertodaleo.app.repository.UserRepository;
import it.albertodaleo.app.security.AuthoritiesConstants;
import it.albertodaleo.app.service.CompanyQueryService;
import it.albertodaleo.app.service.CompanyService;
import it.albertodaleo.app.service.dto.CompanyDTO;
import it.albertodaleo.app.service.mapper.CompanyMapper;
import org.h2.engine.UserBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CompanyResource} REST controller.
 */
@SpringBootTest(classes = SolidalApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CompanyResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_VAT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_VAT_NUMBER = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATION_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CREATION_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_OWNER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_OWNER_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PROVINCE = "AA";
    private static final String UPDATED_PROVINCE = "BB";

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyMapper companyMapper;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyQueryService companyQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompanyMockMvc;

    private Company company;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Company createEntity(EntityManager em) {
        Company company = new Company()
            .name(DEFAULT_NAME)
            .vatNumber(DEFAULT_VAT_NUMBER)
            .creationDate(DEFAULT_CREATION_DATE)
            .address1(DEFAULT_ADDRESS_1)
            .address2(DEFAULT_ADDRESS_2)
            .telephoneNumber(DEFAULT_TELEPHONE_NUMBER)
            .ownerName(DEFAULT_OWNER_NAME)
            .country(DEFAULT_COUNTRY)
            .postalCode(DEFAULT_POSTAL_CODE)
            .province(DEFAULT_PROVINCE);
        return company;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Company createUpdatedEntity(EntityManager em) {
        User user = new User();
        user.setId(25L);

        Company company = new Company()
            .name(UPDATED_NAME)
            .vatNumber(UPDATED_VAT_NUMBER)
            .creationDate(UPDATED_CREATION_DATE)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .telephoneNumber(UPDATED_TELEPHONE_NUMBER)
            .ownerName(UPDATED_OWNER_NAME)
            .country(UPDATED_COUNTRY)
            .postalCode(UPDATED_POSTAL_CODE)
            .province(UPDATED_PROVINCE)
            .user(user);
        return company;
    }

    @BeforeEach
    public void initTest() {
        company = createEntity(em);
    }

    @Test
    @WithMockUser(value = "company", authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void createCompany() throws Exception {
        int databaseSizeBeforeCreate = companyRepository.findAll().size();
        User userCompanyWithNoCompany = new User();
        userCompanyWithNoCompany.setLogin("company");
        userCompanyWithNoCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithNoCompany.setFirstName("Franco");
        userCompanyWithNoCompany.setLastName("Ciccio");
        userCompanyWithNoCompany.setEmail("company@company.com");
        userCompanyWithNoCompany.setImageUrl("");
        userCompanyWithNoCompany.setActivated(true);
        userCompanyWithNoCompany.setActivationKey("");
        userCompanyWithNoCompany.setResetKey("");
        userCompanyWithNoCompany.setResetDate(Instant.now());
        userCompanyWithNoCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithNoCompany.setAuthorities(authorities);
        userCompanyWithNoCompany.setCreatedBy("");
        userCompanyWithNoCompany.setCreatedDate(Instant.now());
        userCompanyWithNoCompany.setLastModifiedBy("");
        userCompanyWithNoCompany.setLastModifiedDate(Instant.now());

        userRepository.save(userCompanyWithNoCompany);

        // Create the Company
        CompanyDTO companyDTO = companyMapper.toDto(company);
        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isCreated());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeCreate + 1);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCompany.getVatNumber()).isEqualTo(DEFAULT_VAT_NUMBER);
        assertThat(testCompany.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testCompany.getAddress1()).isEqualTo(DEFAULT_ADDRESS_1);
        assertThat(testCompany.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testCompany.getTelephoneNumber()).isEqualTo(DEFAULT_TELEPHONE_NUMBER);
        assertThat(testCompany.getOwnerName()).isEqualTo(DEFAULT_OWNER_NAME);
        assertThat(testCompany.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testCompany.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testCompany.getProvince()).isEqualTo(DEFAULT_PROVINCE);
        assertThat(testCompany.getUser()).isEqualTo(userCompanyWithNoCompany);
    }

    @Test
    @WithMockUser(value = "company", authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void shouldNotCreateCompanyIfUserNotActivated() throws Exception {
        int databaseSizeBeforeCreate = companyRepository.findAll().size();
        User userCompanyWithNoCompany = new User();
        userCompanyWithNoCompany.setLogin("company");
        userCompanyWithNoCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithNoCompany.setFirstName("Franco");
        userCompanyWithNoCompany.setLastName("Ciccio");
        userCompanyWithNoCompany.setEmail("company@company.com");
        userCompanyWithNoCompany.setImageUrl("");
        userCompanyWithNoCompany.setActivated(false);
        userCompanyWithNoCompany.setActivationKey("");
        userCompanyWithNoCompany.setResetKey("");
        userCompanyWithNoCompany.setResetDate(Instant.now());
        userCompanyWithNoCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithNoCompany.setAuthorities(authorities);
        userCompanyWithNoCompany.setCreatedBy("");
        userCompanyWithNoCompany.setCreatedDate(Instant.now());
        userCompanyWithNoCompany.setLastModifiedBy("");
        userCompanyWithNoCompany.setLastModifiedDate(Instant.now());

        userRepository.save(userCompanyWithNoCompany);

        // Create the Company
        CompanyDTO companyDTO = companyMapper.toDto(company);
        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(value = "company-not-found", authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void shouldThrowExceptionIfNoUserFound() throws Exception {
        CompanyDTO companyDTO = companyMapper.toDto(company);
        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void createCompanyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = companyRepository.findAll().size();

        // Create the Company with an existing ID
        company.setId(1L);
        CompanyDTO companyDTO = companyMapper.toDto(company);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setName(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkVatNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setVatNumber(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkAddress1IsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setAddress1(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkOwnerNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setOwnerName(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkCountryIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setCountry(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkPostalCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setPostalCode(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkProvinceIsRequired() throws Exception {
        int databaseSizeBeforeTest = companyRepository.findAll().size();
        // set the field null
        company.setProvince(null);

        // Create the Company, which fails.
        CompanyDTO companyDTO = companyMapper.toDto(company);

        restCompanyMockMvc.perform(post("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCompanies() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList
        restCompanyMockMvc.perform(get("/api/companies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(company.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].vatNumber").value(hasItem(DEFAULT_VAT_NUMBER)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].telephoneNumber").value(hasItem(DEFAULT_TELEPHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].ownerName").value(hasItem(DEFAULT_OWNER_NAME)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)));
    }

    @Test
    @Transactional
    public void getCompany() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get the company
        restCompanyMockMvc.perform(get("/api/companies/{id}", company.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(company.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.vatNumber").value(DEFAULT_VAT_NUMBER))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.address1").value(DEFAULT_ADDRESS_1))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2))
            .andExpect(jsonPath("$.telephoneNumber").value(DEFAULT_TELEPHONE_NUMBER))
            .andExpect(jsonPath("$.ownerName").value(DEFAULT_OWNER_NAME))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.province").value(DEFAULT_PROVINCE));
    }


    @Test
    @Transactional
    public void getCompaniesByIdFiltering() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        Long id = company.getId();

        defaultCompanyShouldBeFound("id.equals=" + id);
        defaultCompanyShouldNotBeFound("id.notEquals=" + id);

        defaultCompanyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCompanyShouldNotBeFound("id.greaterThan=" + id);

        defaultCompanyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCompanyShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllCompaniesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where name equals to DEFAULT_NAME
        defaultCompanyShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the companyList where name equals to UPDATED_NAME
        defaultCompanyShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where name not equals to DEFAULT_NAME
        defaultCompanyShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the companyList where name not equals to UPDATED_NAME
        defaultCompanyShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCompanyShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the companyList where name equals to UPDATED_NAME
        defaultCompanyShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where name is not null
        defaultCompanyShouldBeFound("name.specified=true");

        // Get all the companyList where name is null
        defaultCompanyShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where name contains DEFAULT_NAME
        defaultCompanyShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the companyList where name contains UPDATED_NAME
        defaultCompanyShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where name does not contain DEFAULT_NAME
        defaultCompanyShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the companyList where name does not contain UPDATED_NAME
        defaultCompanyShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllCompaniesByVatNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where vatNumber equals to DEFAULT_VAT_NUMBER
        defaultCompanyShouldBeFound("vatNumber.equals=" + DEFAULT_VAT_NUMBER);

        // Get all the companyList where vatNumber equals to UPDATED_VAT_NUMBER
        defaultCompanyShouldNotBeFound("vatNumber.equals=" + UPDATED_VAT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByVatNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where vatNumber not equals to DEFAULT_VAT_NUMBER
        defaultCompanyShouldNotBeFound("vatNumber.notEquals=" + DEFAULT_VAT_NUMBER);

        // Get all the companyList where vatNumber not equals to UPDATED_VAT_NUMBER
        defaultCompanyShouldBeFound("vatNumber.notEquals=" + UPDATED_VAT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByVatNumberIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where vatNumber in DEFAULT_VAT_NUMBER or UPDATED_VAT_NUMBER
        defaultCompanyShouldBeFound("vatNumber.in=" + DEFAULT_VAT_NUMBER + "," + UPDATED_VAT_NUMBER);

        // Get all the companyList where vatNumber equals to UPDATED_VAT_NUMBER
        defaultCompanyShouldNotBeFound("vatNumber.in=" + UPDATED_VAT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByVatNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where vatNumber is not null
        defaultCompanyShouldBeFound("vatNumber.specified=true");

        // Get all the companyList where vatNumber is null
        defaultCompanyShouldNotBeFound("vatNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompaniesByVatNumberContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where vatNumber contains DEFAULT_VAT_NUMBER
        defaultCompanyShouldBeFound("vatNumber.contains=" + DEFAULT_VAT_NUMBER);

        // Get all the companyList where vatNumber contains UPDATED_VAT_NUMBER
        defaultCompanyShouldNotBeFound("vatNumber.contains=" + UPDATED_VAT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByVatNumberNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where vatNumber does not contain DEFAULT_VAT_NUMBER
        defaultCompanyShouldNotBeFound("vatNumber.doesNotContain=" + DEFAULT_VAT_NUMBER);

        // Get all the companyList where vatNumber does not contain UPDATED_VAT_NUMBER
        defaultCompanyShouldBeFound("vatNumber.doesNotContain=" + UPDATED_VAT_NUMBER);
    }


    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate equals to DEFAULT_CREATION_DATE
        defaultCompanyShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the companyList where creationDate equals to UPDATED_CREATION_DATE
        defaultCompanyShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate not equals to DEFAULT_CREATION_DATE
        defaultCompanyShouldNotBeFound("creationDate.notEquals=" + DEFAULT_CREATION_DATE);

        // Get all the companyList where creationDate not equals to UPDATED_CREATION_DATE
        defaultCompanyShouldBeFound("creationDate.notEquals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultCompanyShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the companyList where creationDate equals to UPDATED_CREATION_DATE
        defaultCompanyShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate is not null
        defaultCompanyShouldBeFound("creationDate.specified=true");

        // Get all the companyList where creationDate is null
        defaultCompanyShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate is greater than or equal to DEFAULT_CREATION_DATE
        defaultCompanyShouldBeFound("creationDate.greaterThanOrEqual=" + DEFAULT_CREATION_DATE);

        // Get all the companyList where creationDate is greater than or equal to UPDATED_CREATION_DATE
        defaultCompanyShouldNotBeFound("creationDate.greaterThanOrEqual=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate is less than or equal to DEFAULT_CREATION_DATE
        defaultCompanyShouldBeFound("creationDate.lessThanOrEqual=" + DEFAULT_CREATION_DATE);

        // Get all the companyList where creationDate is less than or equal to SMALLER_CREATION_DATE
        defaultCompanyShouldNotBeFound("creationDate.lessThanOrEqual=" + SMALLER_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsLessThanSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate is less than DEFAULT_CREATION_DATE
        defaultCompanyShouldNotBeFound("creationDate.lessThan=" + DEFAULT_CREATION_DATE);

        // Get all the companyList where creationDate is less than UPDATED_CREATION_DATE
        defaultCompanyShouldBeFound("creationDate.lessThan=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCreationDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where creationDate is greater than DEFAULT_CREATION_DATE
        defaultCompanyShouldNotBeFound("creationDate.greaterThan=" + DEFAULT_CREATION_DATE);

        // Get all the companyList where creationDate is greater than SMALLER_CREATION_DATE
        defaultCompanyShouldBeFound("creationDate.greaterThan=" + SMALLER_CREATION_DATE);
    }


    @Test
    @Transactional
    public void getAllCompaniesByAddress1IsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address1 equals to DEFAULT_ADDRESS_1
        defaultCompanyShouldBeFound("address1.equals=" + DEFAULT_ADDRESS_1);

        // Get all the companyList where address1 equals to UPDATED_ADDRESS_1
        defaultCompanyShouldNotBeFound("address1.equals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address1 not equals to DEFAULT_ADDRESS_1
        defaultCompanyShouldNotBeFound("address1.notEquals=" + DEFAULT_ADDRESS_1);

        // Get all the companyList where address1 not equals to UPDATED_ADDRESS_1
        defaultCompanyShouldBeFound("address1.notEquals=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress1IsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address1 in DEFAULT_ADDRESS_1 or UPDATED_ADDRESS_1
        defaultCompanyShouldBeFound("address1.in=" + DEFAULT_ADDRESS_1 + "," + UPDATED_ADDRESS_1);

        // Get all the companyList where address1 equals to UPDATED_ADDRESS_1
        defaultCompanyShouldNotBeFound("address1.in=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress1IsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address1 is not null
        defaultCompanyShouldBeFound("address1.specified=true");

        // Get all the companyList where address1 is null
        defaultCompanyShouldNotBeFound("address1.specified=false");
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress1ContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address1 contains DEFAULT_ADDRESS_1
        defaultCompanyShouldBeFound("address1.contains=" + DEFAULT_ADDRESS_1);

        // Get all the companyList where address1 contains UPDATED_ADDRESS_1
        defaultCompanyShouldNotBeFound("address1.contains=" + UPDATED_ADDRESS_1);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress1NotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address1 does not contain DEFAULT_ADDRESS_1
        defaultCompanyShouldNotBeFound("address1.doesNotContain=" + DEFAULT_ADDRESS_1);

        // Get all the companyList where address1 does not contain UPDATED_ADDRESS_1
        defaultCompanyShouldBeFound("address1.doesNotContain=" + UPDATED_ADDRESS_1);
    }


    @Test
    @Transactional
    public void getAllCompaniesByAddress2IsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address2 equals to DEFAULT_ADDRESS_2
        defaultCompanyShouldBeFound("address2.equals=" + DEFAULT_ADDRESS_2);

        // Get all the companyList where address2 equals to UPDATED_ADDRESS_2
        defaultCompanyShouldNotBeFound("address2.equals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address2 not equals to DEFAULT_ADDRESS_2
        defaultCompanyShouldNotBeFound("address2.notEquals=" + DEFAULT_ADDRESS_2);

        // Get all the companyList where address2 not equals to UPDATED_ADDRESS_2
        defaultCompanyShouldBeFound("address2.notEquals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress2IsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address2 in DEFAULT_ADDRESS_2 or UPDATED_ADDRESS_2
        defaultCompanyShouldBeFound("address2.in=" + DEFAULT_ADDRESS_2 + "," + UPDATED_ADDRESS_2);

        // Get all the companyList where address2 equals to UPDATED_ADDRESS_2
        defaultCompanyShouldNotBeFound("address2.in=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress2IsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address2 is not null
        defaultCompanyShouldBeFound("address2.specified=true");

        // Get all the companyList where address2 is null
        defaultCompanyShouldNotBeFound("address2.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompaniesByAddress2ContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address2 contains DEFAULT_ADDRESS_2
        defaultCompanyShouldBeFound("address2.contains=" + DEFAULT_ADDRESS_2);

        // Get all the companyList where address2 contains UPDATED_ADDRESS_2
        defaultCompanyShouldNotBeFound("address2.contains=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllCompaniesByAddress2NotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where address2 does not contain DEFAULT_ADDRESS_2
        defaultCompanyShouldNotBeFound("address2.doesNotContain=" + DEFAULT_ADDRESS_2);

        // Get all the companyList where address2 does not contain UPDATED_ADDRESS_2
        defaultCompanyShouldBeFound("address2.doesNotContain=" + UPDATED_ADDRESS_2);
    }


    @Test
    @Transactional
    public void getAllCompaniesByTelephoneNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where telephoneNumber equals to DEFAULT_TELEPHONE_NUMBER
        defaultCompanyShouldBeFound("telephoneNumber.equals=" + DEFAULT_TELEPHONE_NUMBER);

        // Get all the companyList where telephoneNumber equals to UPDATED_TELEPHONE_NUMBER
        defaultCompanyShouldNotBeFound("telephoneNumber.equals=" + UPDATED_TELEPHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByTelephoneNumberIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where telephoneNumber not equals to DEFAULT_TELEPHONE_NUMBER
        defaultCompanyShouldNotBeFound("telephoneNumber.notEquals=" + DEFAULT_TELEPHONE_NUMBER);

        // Get all the companyList where telephoneNumber not equals to UPDATED_TELEPHONE_NUMBER
        defaultCompanyShouldBeFound("telephoneNumber.notEquals=" + UPDATED_TELEPHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByTelephoneNumberIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where telephoneNumber in DEFAULT_TELEPHONE_NUMBER or UPDATED_TELEPHONE_NUMBER
        defaultCompanyShouldBeFound("telephoneNumber.in=" + DEFAULT_TELEPHONE_NUMBER + "," + UPDATED_TELEPHONE_NUMBER);

        // Get all the companyList where telephoneNumber equals to UPDATED_TELEPHONE_NUMBER
        defaultCompanyShouldNotBeFound("telephoneNumber.in=" + UPDATED_TELEPHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByTelephoneNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where telephoneNumber is not null
        defaultCompanyShouldBeFound("telephoneNumber.specified=true");

        // Get all the companyList where telephoneNumber is null
        defaultCompanyShouldNotBeFound("telephoneNumber.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompaniesByTelephoneNumberContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where telephoneNumber contains DEFAULT_TELEPHONE_NUMBER
        defaultCompanyShouldBeFound("telephoneNumber.contains=" + DEFAULT_TELEPHONE_NUMBER);

        // Get all the companyList where telephoneNumber contains UPDATED_TELEPHONE_NUMBER
        defaultCompanyShouldNotBeFound("telephoneNumber.contains=" + UPDATED_TELEPHONE_NUMBER);
    }

    @Test
    @Transactional
    public void getAllCompaniesByTelephoneNumberNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where telephoneNumber does not contain DEFAULT_TELEPHONE_NUMBER
        defaultCompanyShouldNotBeFound("telephoneNumber.doesNotContain=" + DEFAULT_TELEPHONE_NUMBER);

        // Get all the companyList where telephoneNumber does not contain UPDATED_TELEPHONE_NUMBER
        defaultCompanyShouldBeFound("telephoneNumber.doesNotContain=" + UPDATED_TELEPHONE_NUMBER);
    }


    @Test
    @Transactional
    public void getAllCompaniesByOwnerNameIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where ownerName equals to DEFAULT_OWNER_NAME
        defaultCompanyShouldBeFound("ownerName.equals=" + DEFAULT_OWNER_NAME);

        // Get all the companyList where ownerName equals to UPDATED_OWNER_NAME
        defaultCompanyShouldNotBeFound("ownerName.equals=" + UPDATED_OWNER_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByOwnerNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where ownerName not equals to DEFAULT_OWNER_NAME
        defaultCompanyShouldNotBeFound("ownerName.notEquals=" + DEFAULT_OWNER_NAME);

        // Get all the companyList where ownerName not equals to UPDATED_OWNER_NAME
        defaultCompanyShouldBeFound("ownerName.notEquals=" + UPDATED_OWNER_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByOwnerNameIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where ownerName in DEFAULT_OWNER_NAME or UPDATED_OWNER_NAME
        defaultCompanyShouldBeFound("ownerName.in=" + DEFAULT_OWNER_NAME + "," + UPDATED_OWNER_NAME);

        // Get all the companyList where ownerName equals to UPDATED_OWNER_NAME
        defaultCompanyShouldNotBeFound("ownerName.in=" + UPDATED_OWNER_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByOwnerNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where ownerName is not null
        defaultCompanyShouldBeFound("ownerName.specified=true");

        // Get all the companyList where ownerName is null
        defaultCompanyShouldNotBeFound("ownerName.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompaniesByOwnerNameContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where ownerName contains DEFAULT_OWNER_NAME
        defaultCompanyShouldBeFound("ownerName.contains=" + DEFAULT_OWNER_NAME);

        // Get all the companyList where ownerName contains UPDATED_OWNER_NAME
        defaultCompanyShouldNotBeFound("ownerName.contains=" + UPDATED_OWNER_NAME);
    }

    @Test
    @Transactional
    public void getAllCompaniesByOwnerNameNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where ownerName does not contain DEFAULT_OWNER_NAME
        defaultCompanyShouldNotBeFound("ownerName.doesNotContain=" + DEFAULT_OWNER_NAME);

        // Get all the companyList where ownerName does not contain UPDATED_OWNER_NAME
        defaultCompanyShouldBeFound("ownerName.doesNotContain=" + UPDATED_OWNER_NAME);
    }


    @Test
    @Transactional
    public void getAllCompaniesByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where country equals to DEFAULT_COUNTRY
        defaultCompanyShouldBeFound("country.equals=" + DEFAULT_COUNTRY);

        // Get all the companyList where country equals to UPDATED_COUNTRY
        defaultCompanyShouldNotBeFound("country.equals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCountryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where country not equals to DEFAULT_COUNTRY
        defaultCompanyShouldNotBeFound("country.notEquals=" + DEFAULT_COUNTRY);

        // Get all the companyList where country not equals to UPDATED_COUNTRY
        defaultCompanyShouldBeFound("country.notEquals=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCountryIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where country in DEFAULT_COUNTRY or UPDATED_COUNTRY
        defaultCompanyShouldBeFound("country.in=" + DEFAULT_COUNTRY + "," + UPDATED_COUNTRY);

        // Get all the companyList where country equals to UPDATED_COUNTRY
        defaultCompanyShouldNotBeFound("country.in=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCountryIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where country is not null
        defaultCompanyShouldBeFound("country.specified=true");

        // Get all the companyList where country is null
        defaultCompanyShouldNotBeFound("country.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompaniesByCountryContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where country contains DEFAULT_COUNTRY
        defaultCompanyShouldBeFound("country.contains=" + DEFAULT_COUNTRY);

        // Get all the companyList where country contains UPDATED_COUNTRY
        defaultCompanyShouldNotBeFound("country.contains=" + UPDATED_COUNTRY);
    }

    @Test
    @Transactional
    public void getAllCompaniesByCountryNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where country does not contain DEFAULT_COUNTRY
        defaultCompanyShouldNotBeFound("country.doesNotContain=" + DEFAULT_COUNTRY);

        // Get all the companyList where country does not contain UPDATED_COUNTRY
        defaultCompanyShouldBeFound("country.doesNotContain=" + UPDATED_COUNTRY);
    }


    @Test
    @Transactional
    public void getAllCompaniesByPostalCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where postalCode equals to DEFAULT_POSTAL_CODE
        defaultCompanyShouldBeFound("postalCode.equals=" + DEFAULT_POSTAL_CODE);

        // Get all the companyList where postalCode equals to UPDATED_POSTAL_CODE
        defaultCompanyShouldNotBeFound("postalCode.equals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByPostalCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where postalCode not equals to DEFAULT_POSTAL_CODE
        defaultCompanyShouldNotBeFound("postalCode.notEquals=" + DEFAULT_POSTAL_CODE);

        // Get all the companyList where postalCode not equals to UPDATED_POSTAL_CODE
        defaultCompanyShouldBeFound("postalCode.notEquals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByPostalCodeIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where postalCode in DEFAULT_POSTAL_CODE or UPDATED_POSTAL_CODE
        defaultCompanyShouldBeFound("postalCode.in=" + DEFAULT_POSTAL_CODE + "," + UPDATED_POSTAL_CODE);

        // Get all the companyList where postalCode equals to UPDATED_POSTAL_CODE
        defaultCompanyShouldNotBeFound("postalCode.in=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByPostalCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where postalCode is not null
        defaultCompanyShouldBeFound("postalCode.specified=true");

        // Get all the companyList where postalCode is null
        defaultCompanyShouldNotBeFound("postalCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompaniesByPostalCodeContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where postalCode contains DEFAULT_POSTAL_CODE
        defaultCompanyShouldBeFound("postalCode.contains=" + DEFAULT_POSTAL_CODE);

        // Get all the companyList where postalCode contains UPDATED_POSTAL_CODE
        defaultCompanyShouldNotBeFound("postalCode.contains=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByPostalCodeNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where postalCode does not contain DEFAULT_POSTAL_CODE
        defaultCompanyShouldNotBeFound("postalCode.doesNotContain=" + DEFAULT_POSTAL_CODE);

        // Get all the companyList where postalCode does not contain UPDATED_POSTAL_CODE
        defaultCompanyShouldBeFound("postalCode.doesNotContain=" + UPDATED_POSTAL_CODE);
    }


    @Test
    @Transactional
    public void getAllCompaniesByProvinceIsEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where province equals to DEFAULT_PROVINCE
        defaultCompanyShouldBeFound("province.equals=" + DEFAULT_PROVINCE);

        // Get all the companyList where province equals to UPDATED_PROVINCE
        defaultCompanyShouldNotBeFound("province.equals=" + UPDATED_PROVINCE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByProvinceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where province not equals to DEFAULT_PROVINCE
        defaultCompanyShouldNotBeFound("province.notEquals=" + DEFAULT_PROVINCE);

        // Get all the companyList where province not equals to UPDATED_PROVINCE
        defaultCompanyShouldBeFound("province.notEquals=" + UPDATED_PROVINCE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByProvinceIsInShouldWork() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where province in DEFAULT_PROVINCE or UPDATED_PROVINCE
        defaultCompanyShouldBeFound("province.in=" + DEFAULT_PROVINCE + "," + UPDATED_PROVINCE);

        // Get all the companyList where province equals to UPDATED_PROVINCE
        defaultCompanyShouldNotBeFound("province.in=" + UPDATED_PROVINCE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByProvinceIsNullOrNotNull() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where province is not null
        defaultCompanyShouldBeFound("province.specified=true");

        // Get all the companyList where province is null
        defaultCompanyShouldNotBeFound("province.specified=false");
    }
                @Test
    @Transactional
    public void getAllCompaniesByProvinceContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where province contains DEFAULT_PROVINCE
        defaultCompanyShouldBeFound("province.contains=" + DEFAULT_PROVINCE);

        // Get all the companyList where province contains UPDATED_PROVINCE
        defaultCompanyShouldNotBeFound("province.contains=" + UPDATED_PROVINCE);
    }

    @Test
    @Transactional
    public void getAllCompaniesByProvinceNotContainsSomething() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        // Get all the companyList where province does not contain DEFAULT_PROVINCE
        defaultCompanyShouldNotBeFound("province.doesNotContain=" + DEFAULT_PROVINCE);

        // Get all the companyList where province does not contain UPDATED_PROVINCE
        defaultCompanyShouldBeFound("province.doesNotContain=" + UPDATED_PROVINCE);
    }


    @Test
    @Transactional
    @WithMockUser(authorities = AuthoritiesConstants.USER)
    public void shouldBeForbiddenGetOwnedCompaniesByUserIfIsNotCompany() throws Exception {
        // WHEN
        restCompanyMockMvc.perform(get("/api/owned-companies?sort=id,desc"))
            .andExpect(status().isForbidden());
    }


    @Test
    @Transactional
    public void shouldNotBeAuthorizedGetOwnedCompaniesByUserIfIsNotLogged() throws Exception {
        // WHEN
        restCompanyMockMvc.perform(get("/api/owned-companies?sort=id,desc"))
            .andExpect(status().isForbidden());
    }

    @Test
    @Transactional
    @WithMockUser(username = "company", authorities = AuthoritiesConstants.COMPANY)
    public void shouldGetOwnedCompaniesByUserCompany() throws Exception {
        // GIVEN
        User userCompanyWithOneCompany = new User();
        userCompanyWithOneCompany.setLogin("company");
        userCompanyWithOneCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithOneCompany.setFirstName("Franco");
        userCompanyWithOneCompany.setLastName("Ciccio");
        userCompanyWithOneCompany.setEmail("company@company.com");
        userCompanyWithOneCompany.setImageUrl("");
        userCompanyWithOneCompany.setActivated(true);
        userCompanyWithOneCompany.setActivationKey("");
        userCompanyWithOneCompany.setResetKey("");
        userCompanyWithOneCompany.setResetDate(Instant.now());
        userCompanyWithOneCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithOneCompany.setAuthorities(authorities);
        userCompanyWithOneCompany.setCreatedBy("");
        userCompanyWithOneCompany.setCreatedDate(Instant.now());
        userCompanyWithOneCompany.setLastModifiedBy("");
        userCompanyWithOneCompany.setLastModifiedDate(Instant.now());

        User savedUser = userRepository.save(userCompanyWithOneCompany);
        company.user(savedUser);

        companyRepository.saveAndFlush(company);

        // WHEN
        restCompanyMockMvc.perform(get("/api/owned-companies"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(company.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].vatNumber").value(hasItem(DEFAULT_VAT_NUMBER)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].telephoneNumber").value(hasItem(DEFAULT_TELEPHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].ownerName").value(hasItem(DEFAULT_OWNER_NAME)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)))
            .andExpect(jsonPath("$[*].userId").value(hasItem(savedUser.getId().intValue())));
    }

    @Test
    @Transactional
    @WithMockUser(username = "company", authorities = AuthoritiesConstants.COMPANY)
    public void shouldGetOwnedTwoCompaniesByUserCompany() throws Exception {
        // GIVEN
        User userCompanyWithOneCompany = new User();
        userCompanyWithOneCompany.setLogin("company");
        userCompanyWithOneCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithOneCompany.setFirstName("Franco");
        userCompanyWithOneCompany.setLastName("Ciccio");
        userCompanyWithOneCompany.setEmail("company@company.com");
        userCompanyWithOneCompany.setImageUrl("");
        userCompanyWithOneCompany.setActivated(true);
        userCompanyWithOneCompany.setActivationKey("");
        userCompanyWithOneCompany.setResetKey("");
        userCompanyWithOneCompany.setResetDate(Instant.now());
        userCompanyWithOneCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithOneCompany.setAuthorities(authorities);
        userCompanyWithOneCompany.setCreatedBy("");
        userCompanyWithOneCompany.setCreatedDate(Instant.now());
        userCompanyWithOneCompany.setLastModifiedBy("");
        userCompanyWithOneCompany.setLastModifiedDate(Instant.now());

        User savedUser = userRepository.save(userCompanyWithOneCompany);
        company.user(savedUser);

        companyRepository.saveAndFlush(company);

        Company company2 = CompanyBuilder.aDefaultCompany().withUser(savedUser).build();
        companyRepository.saveAndFlush(company2);

        // WHEN
        restCompanyMockMvc.perform(get("/api/owned-companies"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*]").value(hasSize(2)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(savedUser.getId().intValue())));
    }

    @Test
    @Transactional
    @WithMockUser(username = "company", authorities = AuthoritiesConstants.COMPANY)
    public void shouldGetOwnedNoCompaniesByUserCompanyWithoutCompany() throws Exception {
        // GIVEN
        User userCompanyWithNoCompany = new User();
        userCompanyWithNoCompany.setLogin("company");
        userCompanyWithNoCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithNoCompany.setFirstName("Franco");
        userCompanyWithNoCompany.setLastName("Ciccio");
        userCompanyWithNoCompany.setEmail("company@company.com");
        userCompanyWithNoCompany.setImageUrl("");
        userCompanyWithNoCompany.setActivated(true);
        userCompanyWithNoCompany.setActivationKey("");
        userCompanyWithNoCompany.setResetKey("");
        userCompanyWithNoCompany.setResetDate(Instant.now());
        userCompanyWithNoCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithNoCompany.setAuthorities(authorities);
        userCompanyWithNoCompany.setCreatedBy("");
        userCompanyWithNoCompany.setCreatedDate(Instant.now());
        userCompanyWithNoCompany.setLastModifiedBy("");
        userCompanyWithNoCompany.setLastModifiedDate(Instant.now());

        userRepository.save(userCompanyWithNoCompany);

        // WHEN
        restCompanyMockMvc.perform(get("/api/owned-companies"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*]").value(hasSize(0)));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCompanyShouldBeFound(String filter) throws Exception {
        restCompanyMockMvc.perform(get("/api/companies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(company.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].vatNumber").value(hasItem(DEFAULT_VAT_NUMBER)))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].telephoneNumber").value(hasItem(DEFAULT_TELEPHONE_NUMBER)))
            .andExpect(jsonPath("$.[*].ownerName").value(hasItem(DEFAULT_OWNER_NAME)))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].province").value(hasItem(DEFAULT_PROVINCE)));

        // Check, that the count call also returns 1
        restCompanyMockMvc.perform(get("/api/companies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCompanyShouldNotBeFound(String filter) throws Exception {
        restCompanyMockMvc.perform(get("/api/companies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCompanyMockMvc.perform(get("/api/companies/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCompany() throws Exception {
        // Get the company
        restCompanyMockMvc.perform(get("/api/companies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    // FIXME test not passing when run with all the others
   /* @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void updateCompany() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        int databaseSizeBeforeUpdate = companyRepository.findAll().size();

        // Update the company
        Company updatedCompany = companyRepository.findById(company.getId()).get();
        // Disconnect from session so that the updates on updatedCompany are not directly saved in db
        em.detach(updatedCompany);
        updatedCompany
            .name(UPDATED_NAME)
            .vatNumber(UPDATED_VAT_NUMBER)
            .creationDate(UPDATED_CREATION_DATE)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .telephoneNumber(UPDATED_TELEPHONE_NUMBER)
            .ownerName(UPDATED_OWNER_NAME)
            .country(UPDATED_COUNTRY)
            .postalCode(UPDATED_POSTAL_CODE)
            .province(UPDATED_PROVINCE);
        CompanyDTO companyDTO = companyMapper.toDto(updatedCompany);

        restCompanyMockMvc.perform(put("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isOk());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
        Company testCompany = companyList.get(companyList.size() - 1);
        assertThat(testCompany.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCompany.getVatNumber()).isEqualTo(UPDATED_VAT_NUMBER);
        assertThat(testCompany.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testCompany.getAddress1()).isEqualTo(UPDATED_ADDRESS_1);
        assertThat(testCompany.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testCompany.getTelephoneNumber()).isEqualTo(UPDATED_TELEPHONE_NUMBER);
        assertThat(testCompany.getOwnerName()).isEqualTo(UPDATED_OWNER_NAME);
        assertThat(testCompany.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testCompany.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testCompany.getProvince()).isEqualTo(UPDATED_PROVINCE);
    }*/

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void updateNonExistingCompany() throws Exception {
        int databaseSizeBeforeUpdate = companyRepository.findAll().size();

        // Create the Company
        CompanyDTO companyDTO = companyMapper.toDto(company);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompanyMockMvc.perform(put("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @WithMockUser(value = "company", authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void updateExistingCompany() throws Exception {
        User userCompany = new User();
        userCompany.setLogin("company");
        userCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompany.setFirstName("Franco");
        userCompany.setLastName("Ciccio");
        userCompany.setEmail("company@company.com");
        userCompany.setImageUrl("");
        userCompany.setActivated(true);
        userCompany.setActivationKey("");
        userCompany.setResetKey("");
        userCompany.setResetDate(Instant.now());
        userCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompany.setAuthorities(authorities);
        userCompany.setCreatedBy("");
        userCompany.setCreatedDate(Instant.now());
        userCompany.setLastModifiedBy("");
        userCompany.setLastModifiedDate(Instant.now());

        userRepository.save(userCompany);
        company.user(userCompany);
        companyRepository.saveAndFlush(company);
        // Create the Company
        List<Company> byUserIsCurrentUser = companyRepository.findByUserIsCurrentUser();
        int databaseSizeBeforeUpdate = byUserIsCurrentUser.size();

        byUserIsCurrentUser.get(0).name("company-updated");
        CompanyDTO companyDTO = companyMapper.toDto(byUserIsCurrentUser.get(0).name("company-updated"));

        restCompanyMockMvc.perform(put("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isOk());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
        assertThat(companyList.get(0).getName()).isEqualTo("company-updated");
    }

    @Test
    @WithMockUser(value = "company", authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void shouldNotupdateExistingCompanyIfUserNotActivated() throws Exception {
        User userCompany = new User();
        userCompany.setLogin("company");
        userCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompany.setFirstName("Franco");
        userCompany.setLastName("Ciccio");
        userCompany.setEmail("company@company.com");
        userCompany.setImageUrl("");
        userCompany.setActivated(false);
        userCompany.setActivationKey("");
        userCompany.setResetKey("");
        userCompany.setResetDate(Instant.now());
        userCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompany.setAuthorities(authorities);
        userCompany.setCreatedBy("");
        userCompany.setCreatedDate(Instant.now());
        userCompany.setLastModifiedBy("");
        userCompany.setLastModifiedDate(Instant.now());

        userRepository.save(userCompany);
        company.user(userCompany);
        companyRepository.saveAndFlush(company);
        // Create the Company
        List<Company> byUserIsCurrentUser = companyRepository.findByUserIsCurrentUser();
        int databaseSizeBeforeUpdate = byUserIsCurrentUser.size();

        byUserIsCurrentUser.get(0).name("company-updated");
        CompanyDTO companyDTO = companyMapper.toDto(byUserIsCurrentUser.get(0).name("company-updated"));

        restCompanyMockMvc.perform(put("/api/companies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(companyDTO)))
            .andExpect(status().isUnauthorized());

        // Validate the Company in the database
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeUpdate);
    }


    @Test
    @Transactional
    public void deleteCompany() throws Exception {
        // Initialize the database
        companyRepository.saveAndFlush(company);

        int databaseSizeBeforeDelete = companyRepository.findAll().size();

        // Delete the company
        restCompanyMockMvc.perform(delete("/api/companies/{id}", company.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Company> companyList = companyRepository.findAll();
        assertThat(companyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
