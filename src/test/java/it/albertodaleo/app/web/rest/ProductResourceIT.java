package it.albertodaleo.app.web.rest;

import com.google.common.collect.Sets;
import it.albertodaleo.app.SolidalApp;
import it.albertodaleo.app.builder.CompanyBuilder;
import it.albertodaleo.app.domain.*;
import it.albertodaleo.app.domain.enumeration.Category;
import it.albertodaleo.app.domain.enumeration.Currency;
import it.albertodaleo.app.domain.enumeration.Tags;
import it.albertodaleo.app.domain.enumeration.Type;
import it.albertodaleo.app.repository.CompanyRepository;
import it.albertodaleo.app.repository.ProductRepository;
import it.albertodaleo.app.repository.UserRepository;
import it.albertodaleo.app.security.AuthoritiesConstants;
import it.albertodaleo.app.service.ProductQueryService;
import it.albertodaleo.app.service.ProductService;
import it.albertodaleo.app.service.dto.ProductDTO;
import it.albertodaleo.app.service.mapper.ProductMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductResource} REST controller.
 */
@SpringBootTest(classes = SolidalApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ProductResourceIT {

    private static final String DEFAULT_PRODUCT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_NAME = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_PRODUCT_CODE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRODUCT_CODE = new BigDecimal(2);
    private static final BigDecimal SMALLER_PRODUCT_CODE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(2);
    private static final BigDecimal SMALLER_PRICE = new BigDecimal(1 - 1);

    private static final Double DEFAULT_DISCOUNT = 1D;
    private static final Double UPDATED_DISCOUNT = 2D;
    private static final Double SMALLER_DISCOUNT = 1D - 1D;

    private static final Currency DEFAULT_CURRENCY = Currency.EUR;
    private static final Currency UPDATED_CURRENCY = Currency.USD;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB";

    private static final Long DEFAULT_STOCK = 1L;
    private static final Long UPDATED_STOCK = 2L;
    private static final Long SMALLER_STOCK = 1L - 1L;

    private static final Boolean DEFAULT_IS_NEW = false;
    private static final Boolean UPDATED_IS_NEW = true;

    private static final Type DEFAULT_TYPE = Type.VEGETABLE;
    private static final Type UPDATED_TYPE = Type.MEAT;

    private static final String DEFAULT_BRAND = "AAAAAAAAAA";
    private static final String UPDATED_BRAND = "BBBBBBBBBB";

    private static final Category DEFAULT_CATEGORY = Category.FOOD;
    private static final Category UPDATED_CATEGORY = Category.CRAFT;

    private static final Boolean DEFAULT_SALE = false;
    private static final Boolean UPDATED_SALE = true;

    private static final Tags DEFAULT_TAGS = Tags.ORGANIC;
    private static final Tags UPDATED_TAGS = Tags.KM0;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductQueryService productQueryService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductMockMvc;

    private Product product;

    /**
     * Create an entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Product createEntity(EntityManager em) {
        Product product = new Product()
            .productName(DEFAULT_PRODUCT_NAME)
            .productCode(DEFAULT_PRODUCT_CODE)
            .price(DEFAULT_PRICE)
            .discount(DEFAULT_DISCOUNT)
            .currency(DEFAULT_CURRENCY)
            .description(DEFAULT_DESCRIPTION)
            .stock(DEFAULT_STOCK)
            .isNew(DEFAULT_IS_NEW)
            .type(DEFAULT_TYPE)
            .brand(DEFAULT_BRAND)
            .category(DEFAULT_CATEGORY)
            .sale(DEFAULT_SALE)
            .tags(DEFAULT_TAGS);
        return product;
    }

    /**
     * Create an updated entity for this test.
     * <p>
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Product createUpdatedEntity(EntityManager em) {
        Product product = new Product()
            .productName(UPDATED_PRODUCT_NAME)
            .productCode(UPDATED_PRODUCT_CODE)
            .price(UPDATED_PRICE)
            .discount(UPDATED_DISCOUNT)
            .currency(UPDATED_CURRENCY)
            .description(UPDATED_DESCRIPTION)
            .stock(UPDATED_STOCK)
            .isNew(UPDATED_IS_NEW)
            .type(UPDATED_TYPE)
            .brand(UPDATED_BRAND)
            .category(UPDATED_CATEGORY)
            .sale(UPDATED_SALE)
            .tags(UPDATED_TAGS);
        return product;
    }

    @BeforeEach
    public void initTest() {
        product = createEntity(em);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void createProduct() throws Exception {
        int databaseSizeBeforeCreate = productRepository.findAll().size();

        // Create the Product
        ProductDTO productDTO = productMapper.toDto(product);
        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isCreated());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeCreate + 1);
        Product testProduct = productList.get(productList.size() - 1);
        assertThat(testProduct.getProductName()).isEqualTo(DEFAULT_PRODUCT_NAME);
        assertThat(testProduct.getProductCode()).isEqualTo(DEFAULT_PRODUCT_CODE);
        assertThat(testProduct.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testProduct.getDiscount()).isEqualTo(DEFAULT_DISCOUNT);
        assertThat(testProduct.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(testProduct.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProduct.getStock()).isEqualTo(DEFAULT_STOCK);
        assertThat(testProduct.isIsNew()).isEqualTo(DEFAULT_IS_NEW);
        assertThat(testProduct.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testProduct.getBrand()).isEqualTo(DEFAULT_BRAND);
        assertThat(testProduct.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testProduct.isSale()).isEqualTo(DEFAULT_SALE);
        assertThat(testProduct.getTags()).isEqualTo(DEFAULT_TAGS);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void createProductWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productRepository.findAll().size();

        // Create the Product with an existing ID
        product.setId(1L);
        ProductDTO productDTO = productMapper.toDto(product);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkProductNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRepository.findAll().size();
        // set the field null
        product.setProductName(null);

        // Create the Product, which fails.
        ProductDTO productDTO = productMapper.toDto(product);

        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkProductCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRepository.findAll().size();
        // set the field null
        product.setProductCode(null);

        // Create the Product, which fails.
        ProductDTO productDTO = productMapper.toDto(product);

        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = AuthoritiesConstants.COMPANY)
    @Transactional
    public void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRepository.findAll().size();
        // set the field null
        product.setPrice(null);

        // Create the Product, which fails.
        ProductDTO productDTO = productMapper.toDto(product);

        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = {AuthoritiesConstants.COMPANY})
    @Transactional
    public void checkCurrencyIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRepository.findAll().size();
        // set the field null
        product.setCurrency(null);

        // Create the Product, which fails.
        ProductDTO productDTO = productMapper.toDto(product);

        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = {AuthoritiesConstants.COMPANY})
    @Transactional
    public void checkStockIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRepository.findAll().size();
        // set the field null
        product.setStock(null);

        // Create the Product, which fails.
        ProductDTO productDTO = productMapper.toDto(product);

        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = {AuthoritiesConstants.COMPANY})
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRepository.findAll().size();
        // set the field null
        product.setType(null);

        // Create the Product, which fails.
        ProductDTO productDTO = productMapper.toDto(product);

        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @WithMockUser(authorities = {AuthoritiesConstants.COMPANY})
    @Transactional
    public void checkCategoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = productRepository.findAll().size();
        // set the field null
        product.setCategory(null);

        // Create the Product, which fails.
        ProductDTO productDTO = productMapper.toDto(product);

        restProductMockMvc.perform(post("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProducts() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].productName").value(hasItem(DEFAULT_PRODUCT_NAME)))
            .andExpect(jsonPath("$.[*].productCode").value(hasItem(DEFAULT_PRODUCT_CODE.intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].stock").value(hasItem(DEFAULT_STOCK.intValue())))
            .andExpect(jsonPath("$.[*].isNew").value(hasItem(DEFAULT_IS_NEW.booleanValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].brand").value(hasItem(DEFAULT_BRAND)))
            .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].sale").value(hasItem(DEFAULT_SALE.booleanValue())))
            .andExpect(jsonPath("$.[*].tags").value(hasItem(DEFAULT_TAGS.toString())));
    }

    @Test
    @Transactional
    public void getAllProductsThatContainsCompanyIds() throws Exception {
        // Initialize the database
        User userCompanyWithOneCompany = new User();
        userCompanyWithOneCompany.setLogin("company");
        userCompanyWithOneCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithOneCompany.setFirstName("Franco");
        userCompanyWithOneCompany.setLastName("Ciccio");
        userCompanyWithOneCompany.setEmail("company@company.com");
        userCompanyWithOneCompany.setImageUrl("");
        userCompanyWithOneCompany.setActivated(true);
        userCompanyWithOneCompany.setActivationKey("");
        userCompanyWithOneCompany.setResetKey("");
        userCompanyWithOneCompany.setResetDate(Instant.now());
        userCompanyWithOneCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithOneCompany.setAuthorities(authorities);
        userCompanyWithOneCompany.setCreatedBy("");
        userCompanyWithOneCompany.setCreatedDate(Instant.now());
        userCompanyWithOneCompany.setLastModifiedBy("");
        userCompanyWithOneCompany.setLastModifiedDate(Instant.now());

        User savedUser = userRepository.saveAndFlush(userCompanyWithOneCompany);

        Company savedCompany = companyRepository.saveAndFlush(CompanyBuilder.aDefaultCompany().withUser(savedUser).build());

        product.setCompany(savedCompany);
        productRepository.saveAndFlush(product);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products?companyId.in=" + savedCompany.getId() + "&sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].productName").value(hasItem(DEFAULT_PRODUCT_NAME)))
            .andExpect(jsonPath("$.[*].productCode").value(hasItem(DEFAULT_PRODUCT_CODE.intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].companyId").value(hasItem(savedCompany.getId().intValue())));
    }

    @Test
    @Transactional
    public void shouldNotGetAllProductsThatContainsANotExistingCompanyId() throws Exception {
        // Initialize the database
        User userCompanyWithOneCompany = new User();
        userCompanyWithOneCompany.setLogin("company");
        userCompanyWithOneCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithOneCompany.setFirstName("Franco");
        userCompanyWithOneCompany.setLastName("Ciccio");
        userCompanyWithOneCompany.setEmail("company@company.com");
        userCompanyWithOneCompany.setImageUrl("");
        userCompanyWithOneCompany.setActivated(true);
        userCompanyWithOneCompany.setActivationKey("");
        userCompanyWithOneCompany.setResetKey("");
        userCompanyWithOneCompany.setResetDate(Instant.now());
        userCompanyWithOneCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithOneCompany.setAuthorities(authorities);
        userCompanyWithOneCompany.setCreatedBy("");
        userCompanyWithOneCompany.setCreatedDate(Instant.now());
        userCompanyWithOneCompany.setLastModifiedBy("");
        userCompanyWithOneCompany.setLastModifiedDate(Instant.now());

        User savedUser = userRepository.saveAndFlush(userCompanyWithOneCompany);

        Company savedCompany = companyRepository.saveAndFlush(CompanyBuilder.aDefaultCompany().withUser(savedUser).build());

        product.setCompany(savedCompany);
        productRepository.saveAndFlush(product);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products?companyId.in=999&sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*]").isEmpty());
    }

    @Test
    @WithMockUser(value = "company", authorities = {AuthoritiesConstants.COMPANY, AuthoritiesConstants.USER})
    @Transactional
    public void getAllProductsPerCompany() throws Exception {
        // Initialize the database
        User userCompanyWithOneCompany = new User();
        userCompanyWithOneCompany.setLogin("company");
        userCompanyWithOneCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithOneCompany.setFirstName("Franco");
        userCompanyWithOneCompany.setLastName("Ciccio");
        userCompanyWithOneCompany.setEmail("company@company.com");
        userCompanyWithOneCompany.setImageUrl("");
        userCompanyWithOneCompany.setActivated(true);
        userCompanyWithOneCompany.setActivationKey("");
        userCompanyWithOneCompany.setResetKey("");
        userCompanyWithOneCompany.setResetDate(Instant.now());
        userCompanyWithOneCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);

        Authority companyRole = new Authority();
        companyRole.setName(AuthoritiesConstants.COMPANY);
        authorities.add(companyRole);

        userCompanyWithOneCompany.setAuthorities(authorities);
        userCompanyWithOneCompany.setCreatedBy("");
        userCompanyWithOneCompany.setCreatedDate(Instant.now());
        userCompanyWithOneCompany.setLastModifiedBy("");
        userCompanyWithOneCompany.setLastModifiedDate(Instant.now());

        User savedUser = userRepository.saveAndFlush(userCompanyWithOneCompany);

        product.setCompany(companyRepository.saveAndFlush(CompanyBuilder.aDefaultCompany().withUser(savedUser).build()));
        productRepository.saveAndFlush(product);

        Product productToSave = ProductBuilder
            .aProduct()
            .withCategory(Category.CRAFT)
            .withType(Type.MEAT)
            .withCurrency(Currency.EUR)
            .withDescription("description interesting asdasd asdasdas das das dasd as")
            .withCompany(companyRepository.saveAndFlush(CompanyBuilder
                .aDefaultCompany()
                .withName("company2")
                .withUser(savedUser)
                .build()))
            .withProductCode(BigDecimal.valueOf(123))
            .withPrice(BigDecimal.valueOf(100))
            .withProductName("product2")
            .withStock(0L)
            .build();

        Product product2 = productRepository.saveAndFlush(productToSave);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products-per-company?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product2.getId().intValue())))
            .andExpect(jsonPath("$.[*].productName").value(hasItem("product2")))
            .andExpect(jsonPath("$.[*].productCode").value(hasItem(123)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(100)))
            .andExpect(jsonPath("$.[*].companyId").value(hasItem(product2.getCompany().getId().intValue())))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].productName").value(hasItem(DEFAULT_PRODUCT_NAME)))
            .andExpect(jsonPath("$.[*].productCode").value(hasItem(DEFAULT_PRODUCT_CODE.intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].companyId").value(hasItem(product.getCompany().getId().intValue())));
    }

    @Test
    @WithMockUser(value = "user-test", authorities = {AuthoritiesConstants.USER})
    @Transactional
    public void shouldNotGetCompaniesIfNotAuthorized() throws Exception {
        // Initialize the database
        User userCompanyWithOneCompany = new User();
        userCompanyWithOneCompany.setLogin("user-test");
        userCompanyWithOneCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithOneCompany.setFirstName("Franco");
        userCompanyWithOneCompany.setLastName("Ciccio");
        userCompanyWithOneCompany.setEmail("company@company.com");
        userCompanyWithOneCompany.setImageUrl("");
        userCompanyWithOneCompany.setActivated(true);
        userCompanyWithOneCompany.setActivationKey("");
        userCompanyWithOneCompany.setResetKey("");
        userCompanyWithOneCompany.setResetDate(Instant.now());
        userCompanyWithOneCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);
        userCompanyWithOneCompany.setAuthorities(authorities);
        userCompanyWithOneCompany.setCreatedBy("");
        userCompanyWithOneCompany.setCreatedDate(Instant.now());
        userCompanyWithOneCompany.setLastModifiedBy("");
        userCompanyWithOneCompany.setLastModifiedDate(Instant.now());

        User savedUser = userRepository.saveAndFlush(userCompanyWithOneCompany);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products-per-company?sort=id,desc"))
            .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(value = "user-test", authorities = {AuthoritiesConstants.USER, AuthoritiesConstants.COMPANY})
    @Transactional
    public void shouldNotGetCompaniesIfNotActivated() throws Exception {
        // Initialize the database
        User userCompanyWithOneCompany = new User();
        userCompanyWithOneCompany.setLogin("user-test");
        userCompanyWithOneCompany.setPassword("123456789012345678901234567890123456789012345678901234567890");
        userCompanyWithOneCompany.setFirstName("Franco");
        userCompanyWithOneCompany.setLastName("Ciccio");
        userCompanyWithOneCompany.setEmail("company@company.com");
        userCompanyWithOneCompany.setImageUrl("");
        userCompanyWithOneCompany.setActivated(false);
        userCompanyWithOneCompany.setActivationKey("");
        userCompanyWithOneCompany.setResetKey("");
        userCompanyWithOneCompany.setResetDate(Instant.now());
        userCompanyWithOneCompany.setLangKey("1234567890");
        HashSet<Authority> authorities = Sets.newHashSet();
        Authority userRole = new Authority();
        userRole.setName(AuthoritiesConstants.USER);
        authorities.add(userRole);
        userCompanyWithOneCompany.setAuthorities(authorities);
        userCompanyWithOneCompany.setCreatedBy("");
        userCompanyWithOneCompany.setCreatedDate(Instant.now());
        userCompanyWithOneCompany.setLastModifiedBy("");
        userCompanyWithOneCompany.setLastModifiedDate(Instant.now());

        User savedUser = userRepository.saveAndFlush(userCompanyWithOneCompany);

        // Get all the productList
        restProductMockMvc.perform(get("/api/products-per-company?sort=id,desc"))
            .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    public void getProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get the product
        restProductMockMvc.perform(get("/api/products/{id}", product.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(product.getId().intValue()))
            .andExpect(jsonPath("$.productName").value(DEFAULT_PRODUCT_NAME))
            .andExpect(jsonPath("$.productCode").value(DEFAULT_PRODUCT_CODE.intValue()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE.intValue()))
            .andExpect(jsonPath("$.discount").value(DEFAULT_DISCOUNT.doubleValue()))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.stock").value(DEFAULT_STOCK.intValue()))
            .andExpect(jsonPath("$.isNew").value(DEFAULT_IS_NEW.booleanValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.brand").value(DEFAULT_BRAND))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()))
            .andExpect(jsonPath("$.sale").value(DEFAULT_SALE.booleanValue()))
            .andExpect(jsonPath("$.tags").value(DEFAULT_TAGS.toString()));
    }


    @Test
    @Transactional
    public void getProductsByIdFiltering() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        Long id = product.getId();

        defaultProductShouldBeFound("id.equals=" + id);
        defaultProductShouldNotBeFound("id.notEquals=" + id);

        defaultProductShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultProductShouldNotBeFound("id.greaterThan=" + id);

        defaultProductShouldBeFound("id.lessThanOrEqual=" + id);
        defaultProductShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllProductsByProductNameIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productName equals to DEFAULT_PRODUCT_NAME
        defaultProductShouldBeFound("productName.equals=" + DEFAULT_PRODUCT_NAME);

        // Get all the productList where productName equals to UPDATED_PRODUCT_NAME
        defaultProductShouldNotBeFound("productName.equals=" + UPDATED_PRODUCT_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByProductNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productName not equals to DEFAULT_PRODUCT_NAME
        defaultProductShouldNotBeFound("productName.notEquals=" + DEFAULT_PRODUCT_NAME);

        // Get all the productList where productName not equals to UPDATED_PRODUCT_NAME
        defaultProductShouldBeFound("productName.notEquals=" + UPDATED_PRODUCT_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByProductNameIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productName in DEFAULT_PRODUCT_NAME or UPDATED_PRODUCT_NAME
        defaultProductShouldBeFound("productName.in=" + DEFAULT_PRODUCT_NAME + "," + UPDATED_PRODUCT_NAME);

        // Get all the productList where productName equals to UPDATED_PRODUCT_NAME
        defaultProductShouldNotBeFound("productName.in=" + UPDATED_PRODUCT_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByProductNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productName is not null
        defaultProductShouldBeFound("productName.specified=true");

        // Get all the productList where productName is null
        defaultProductShouldNotBeFound("productName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByProductNameContainsSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productName contains DEFAULT_PRODUCT_NAME
        defaultProductShouldBeFound("productName.contains=" + DEFAULT_PRODUCT_NAME);

        // Get all the productList where productName contains UPDATED_PRODUCT_NAME
        defaultProductShouldNotBeFound("productName.contains=" + UPDATED_PRODUCT_NAME);
    }

    @Test
    @Transactional
    public void getAllProductsByProductNameNotContainsSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productName does not contain DEFAULT_PRODUCT_NAME
        defaultProductShouldNotBeFound("productName.doesNotContain=" + DEFAULT_PRODUCT_NAME);

        // Get all the productList where productName does not contain UPDATED_PRODUCT_NAME
        defaultProductShouldBeFound("productName.doesNotContain=" + UPDATED_PRODUCT_NAME);
    }


    @Test
    @Transactional
    public void getAllProductsByProductCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode equals to DEFAULT_PRODUCT_CODE
        defaultProductShouldBeFound("productCode.equals=" + DEFAULT_PRODUCT_CODE);

        // Get all the productList where productCode equals to UPDATED_PRODUCT_CODE
        defaultProductShouldNotBeFound("productCode.equals=" + UPDATED_PRODUCT_CODE);
    }

    @Test
    @Transactional
    public void getAllProductsByProductCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode not equals to DEFAULT_PRODUCT_CODE
        defaultProductShouldNotBeFound("productCode.notEquals=" + DEFAULT_PRODUCT_CODE);

        // Get all the productList where productCode not equals to UPDATED_PRODUCT_CODE
        defaultProductShouldBeFound("productCode.notEquals=" + UPDATED_PRODUCT_CODE);
    }

    @Test
    @Transactional
    public void getAllProductsByProductCodeIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode in DEFAULT_PRODUCT_CODE or UPDATED_PRODUCT_CODE
        defaultProductShouldBeFound("productCode.in=" + DEFAULT_PRODUCT_CODE + "," + UPDATED_PRODUCT_CODE);

        // Get all the productList where productCode equals to UPDATED_PRODUCT_CODE
        defaultProductShouldNotBeFound("productCode.in=" + UPDATED_PRODUCT_CODE);
    }

    @Test
    @Transactional
    public void getAllProductsByProductCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode is not null
        defaultProductShouldBeFound("productCode.specified=true");

        // Get all the productList where productCode is null
        defaultProductShouldNotBeFound("productCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByProductCodeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode is greater than or equal to DEFAULT_PRODUCT_CODE
        defaultProductShouldBeFound("productCode.greaterThanOrEqual=" + DEFAULT_PRODUCT_CODE);

        // Get all the productList where productCode is greater than or equal to UPDATED_PRODUCT_CODE
        defaultProductShouldNotBeFound("productCode.greaterThanOrEqual=" + UPDATED_PRODUCT_CODE);
    }

    @Test
    @Transactional
    public void getAllProductsByProductCodeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode is less than or equal to DEFAULT_PRODUCT_CODE
        defaultProductShouldBeFound("productCode.lessThanOrEqual=" + DEFAULT_PRODUCT_CODE);

        // Get all the productList where productCode is less than or equal to SMALLER_PRODUCT_CODE
        defaultProductShouldNotBeFound("productCode.lessThanOrEqual=" + SMALLER_PRODUCT_CODE);
    }

    @Test
    @Transactional
    public void getAllProductsByProductCodeIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode is less than DEFAULT_PRODUCT_CODE
        defaultProductShouldNotBeFound("productCode.lessThan=" + DEFAULT_PRODUCT_CODE);

        // Get all the productList where productCode is less than UPDATED_PRODUCT_CODE
        defaultProductShouldBeFound("productCode.lessThan=" + UPDATED_PRODUCT_CODE);
    }

    @Test
    @Transactional
    public void getAllProductsByProductCodeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where productCode is greater than DEFAULT_PRODUCT_CODE
        defaultProductShouldNotBeFound("productCode.greaterThan=" + DEFAULT_PRODUCT_CODE);

        // Get all the productList where productCode is greater than SMALLER_PRODUCT_CODE
        defaultProductShouldBeFound("productCode.greaterThan=" + SMALLER_PRODUCT_CODE);
    }


    @Test
    @Transactional
    public void getAllProductsByPriceIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price equals to DEFAULT_PRICE
        defaultProductShouldBeFound("price.equals=" + DEFAULT_PRICE);

        // Get all the productList where price equals to UPDATED_PRICE
        defaultProductShouldNotBeFound("price.equals=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price not equals to DEFAULT_PRICE
        defaultProductShouldNotBeFound("price.notEquals=" + DEFAULT_PRICE);

        // Get all the productList where price not equals to UPDATED_PRICE
        defaultProductShouldBeFound("price.notEquals=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price in DEFAULT_PRICE or UPDATED_PRICE
        defaultProductShouldBeFound("price.in=" + DEFAULT_PRICE + "," + UPDATED_PRICE);

        // Get all the productList where price equals to UPDATED_PRICE
        defaultProductShouldNotBeFound("price.in=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price is not null
        defaultProductShouldBeFound("price.specified=true");

        // Get all the productList where price is null
        defaultProductShouldNotBeFound("price.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price is greater than or equal to DEFAULT_PRICE
        defaultProductShouldBeFound("price.greaterThanOrEqual=" + DEFAULT_PRICE);

        // Get all the productList where price is greater than or equal to UPDATED_PRICE
        defaultProductShouldNotBeFound("price.greaterThanOrEqual=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price is less than or equal to DEFAULT_PRICE
        defaultProductShouldBeFound("price.lessThanOrEqual=" + DEFAULT_PRICE);

        // Get all the productList where price is less than or equal to SMALLER_PRICE
        defaultProductShouldNotBeFound("price.lessThanOrEqual=" + SMALLER_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price is less than DEFAULT_PRICE
        defaultProductShouldNotBeFound("price.lessThan=" + DEFAULT_PRICE);

        // Get all the productList where price is less than UPDATED_PRICE
        defaultProductShouldBeFound("price.lessThan=" + UPDATED_PRICE);
    }

    @Test
    @Transactional
    public void getAllProductsByPriceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where price is greater than DEFAULT_PRICE
        defaultProductShouldNotBeFound("price.greaterThan=" + DEFAULT_PRICE);

        // Get all the productList where price is greater than SMALLER_PRICE
        defaultProductShouldBeFound("price.greaterThan=" + SMALLER_PRICE);
    }


    @Test
    @Transactional
    public void getAllProductsByDiscountIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount equals to DEFAULT_DISCOUNT
        defaultProductShouldBeFound("discount.equals=" + DEFAULT_DISCOUNT);

        // Get all the productList where discount equals to UPDATED_DISCOUNT
        defaultProductShouldNotBeFound("discount.equals=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByDiscountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount not equals to DEFAULT_DISCOUNT
        defaultProductShouldNotBeFound("discount.notEquals=" + DEFAULT_DISCOUNT);

        // Get all the productList where discount not equals to UPDATED_DISCOUNT
        defaultProductShouldBeFound("discount.notEquals=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByDiscountIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount in DEFAULT_DISCOUNT or UPDATED_DISCOUNT
        defaultProductShouldBeFound("discount.in=" + DEFAULT_DISCOUNT + "," + UPDATED_DISCOUNT);

        // Get all the productList where discount equals to UPDATED_DISCOUNT
        defaultProductShouldNotBeFound("discount.in=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByDiscountIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount is not null
        defaultProductShouldBeFound("discount.specified=true");

        // Get all the productList where discount is null
        defaultProductShouldNotBeFound("discount.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByDiscountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount is greater than or equal to DEFAULT_DISCOUNT
        defaultProductShouldBeFound("discount.greaterThanOrEqual=" + DEFAULT_DISCOUNT);

        // Get all the productList where discount is greater than or equal to UPDATED_DISCOUNT
        defaultProductShouldNotBeFound("discount.greaterThanOrEqual=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByDiscountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount is less than or equal to DEFAULT_DISCOUNT
        defaultProductShouldBeFound("discount.lessThanOrEqual=" + DEFAULT_DISCOUNT);

        // Get all the productList where discount is less than or equal to SMALLER_DISCOUNT
        defaultProductShouldNotBeFound("discount.lessThanOrEqual=" + SMALLER_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByDiscountIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount is less than DEFAULT_DISCOUNT
        defaultProductShouldNotBeFound("discount.lessThan=" + DEFAULT_DISCOUNT);

        // Get all the productList where discount is less than UPDATED_DISCOUNT
        defaultProductShouldBeFound("discount.lessThan=" + UPDATED_DISCOUNT);
    }

    @Test
    @Transactional
    public void getAllProductsByDiscountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where discount is greater than DEFAULT_DISCOUNT
        defaultProductShouldNotBeFound("discount.greaterThan=" + DEFAULT_DISCOUNT);

        // Get all the productList where discount is greater than SMALLER_DISCOUNT
        defaultProductShouldBeFound("discount.greaterThan=" + SMALLER_DISCOUNT);
    }


    @Test
    @Transactional
    public void getAllProductsByCurrencyIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where currency equals to DEFAULT_CURRENCY
        defaultProductShouldBeFound("currency.equals=" + DEFAULT_CURRENCY);

        // Get all the productList where currency equals to UPDATED_CURRENCY
        defaultProductShouldNotBeFound("currency.equals=" + UPDATED_CURRENCY);
    }

    @Test
    @Transactional
    public void getAllProductsByCurrencyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where currency not equals to DEFAULT_CURRENCY
        defaultProductShouldNotBeFound("currency.notEquals=" + DEFAULT_CURRENCY);

        // Get all the productList where currency not equals to UPDATED_CURRENCY
        defaultProductShouldBeFound("currency.notEquals=" + UPDATED_CURRENCY);
    }

    @Test
    @Transactional
    public void getAllProductsByCurrencyIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where currency in DEFAULT_CURRENCY or UPDATED_CURRENCY
        defaultProductShouldBeFound("currency.in=" + DEFAULT_CURRENCY + "," + UPDATED_CURRENCY);

        // Get all the productList where currency equals to UPDATED_CURRENCY
        defaultProductShouldNotBeFound("currency.in=" + UPDATED_CURRENCY);
    }

    @Test
    @Transactional
    public void getAllProductsByCurrencyIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where currency is not null
        defaultProductShouldBeFound("currency.specified=true");

        // Get all the productList where currency is null
        defaultProductShouldNotBeFound("currency.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description equals to DEFAULT_DESCRIPTION
        defaultProductShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the productList where description equals to UPDATED_DESCRIPTION
        defaultProductShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description not equals to DEFAULT_DESCRIPTION
        defaultProductShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the productList where description not equals to UPDATED_DESCRIPTION
        defaultProductShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultProductShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the productList where description equals to UPDATED_DESCRIPTION
        defaultProductShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description is not null
        defaultProductShouldBeFound("description.specified=true");

        // Get all the productList where description is null
        defaultProductShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description contains DEFAULT_DESCRIPTION
        defaultProductShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the productList where description contains UPDATED_DESCRIPTION
        defaultProductShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProductsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where description does not contain DEFAULT_DESCRIPTION
        defaultProductShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the productList where description does not contain UPDATED_DESCRIPTION
        defaultProductShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllProductsByStockIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock equals to DEFAULT_STOCK
        defaultProductShouldBeFound("stock.equals=" + DEFAULT_STOCK);

        // Get all the productList where stock equals to UPDATED_STOCK
        defaultProductShouldNotBeFound("stock.equals=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock not equals to DEFAULT_STOCK
        defaultProductShouldNotBeFound("stock.notEquals=" + DEFAULT_STOCK);

        // Get all the productList where stock not equals to UPDATED_STOCK
        defaultProductShouldBeFound("stock.notEquals=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock in DEFAULT_STOCK or UPDATED_STOCK
        defaultProductShouldBeFound("stock.in=" + DEFAULT_STOCK + "," + UPDATED_STOCK);

        // Get all the productList where stock equals to UPDATED_STOCK
        defaultProductShouldNotBeFound("stock.in=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is not null
        defaultProductShouldBeFound("stock.specified=true");

        // Get all the productList where stock is null
        defaultProductShouldNotBeFound("stock.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is greater than or equal to DEFAULT_STOCK
        defaultProductShouldBeFound("stock.greaterThanOrEqual=" + DEFAULT_STOCK);

        // Get all the productList where stock is greater than or equal to UPDATED_STOCK
        defaultProductShouldNotBeFound("stock.greaterThanOrEqual=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is less than or equal to DEFAULT_STOCK
        defaultProductShouldBeFound("stock.lessThanOrEqual=" + DEFAULT_STOCK);

        // Get all the productList where stock is less than or equal to SMALLER_STOCK
        defaultProductShouldNotBeFound("stock.lessThanOrEqual=" + SMALLER_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsLessThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is less than DEFAULT_STOCK
        defaultProductShouldNotBeFound("stock.lessThan=" + DEFAULT_STOCK);

        // Get all the productList where stock is less than UPDATED_STOCK
        defaultProductShouldBeFound("stock.lessThan=" + UPDATED_STOCK);
    }

    @Test
    @Transactional
    public void getAllProductsByStockIsGreaterThanSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where stock is greater than DEFAULT_STOCK
        defaultProductShouldNotBeFound("stock.greaterThan=" + DEFAULT_STOCK);

        // Get all the productList where stock is greater than SMALLER_STOCK
        defaultProductShouldBeFound("stock.greaterThan=" + SMALLER_STOCK);
    }


    @Test
    @Transactional
    public void getAllProductsByIsNewIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where isNew equals to DEFAULT_IS_NEW
        defaultProductShouldBeFound("isNew.equals=" + DEFAULT_IS_NEW);

        // Get all the productList where isNew equals to UPDATED_IS_NEW
        defaultProductShouldNotBeFound("isNew.equals=" + UPDATED_IS_NEW);
    }

    @Test
    @Transactional
    public void getAllProductsByIsNewIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where isNew not equals to DEFAULT_IS_NEW
        defaultProductShouldNotBeFound("isNew.notEquals=" + DEFAULT_IS_NEW);

        // Get all the productList where isNew not equals to UPDATED_IS_NEW
        defaultProductShouldBeFound("isNew.notEquals=" + UPDATED_IS_NEW);
    }

    @Test
    @Transactional
    public void getAllProductsByIsNewIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where isNew in DEFAULT_IS_NEW or UPDATED_IS_NEW
        defaultProductShouldBeFound("isNew.in=" + DEFAULT_IS_NEW + "," + UPDATED_IS_NEW);

        // Get all the productList where isNew equals to UPDATED_IS_NEW
        defaultProductShouldNotBeFound("isNew.in=" + UPDATED_IS_NEW);
    }

    @Test
    @Transactional
    public void getAllProductsByIsNewIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where isNew is not null
        defaultProductShouldBeFound("isNew.specified=true");

        // Get all the productList where isNew is null
        defaultProductShouldNotBeFound("isNew.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where type equals to DEFAULT_TYPE
        defaultProductShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the productList where type equals to UPDATED_TYPE
        defaultProductShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllProductsByTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where type not equals to DEFAULT_TYPE
        defaultProductShouldNotBeFound("type.notEquals=" + DEFAULT_TYPE);

        // Get all the productList where type not equals to UPDATED_TYPE
        defaultProductShouldBeFound("type.notEquals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllProductsByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultProductShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the productList where type equals to UPDATED_TYPE
        defaultProductShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllProductsByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where type is not null
        defaultProductShouldBeFound("type.specified=true");

        // Get all the productList where type is null
        defaultProductShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByBrandIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where brand equals to DEFAULT_BRAND
        defaultProductShouldBeFound("brand.equals=" + DEFAULT_BRAND);

        // Get all the productList where brand equals to UPDATED_BRAND
        defaultProductShouldNotBeFound("brand.equals=" + UPDATED_BRAND);
    }

    @Test
    @Transactional
    public void getAllProductsByBrandIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where brand not equals to DEFAULT_BRAND
        defaultProductShouldNotBeFound("brand.notEquals=" + DEFAULT_BRAND);

        // Get all the productList where brand not equals to UPDATED_BRAND
        defaultProductShouldBeFound("brand.notEquals=" + UPDATED_BRAND);
    }

    @Test
    @Transactional
    public void getAllProductsByBrandIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where brand in DEFAULT_BRAND or UPDATED_BRAND
        defaultProductShouldBeFound("brand.in=" + DEFAULT_BRAND + "," + UPDATED_BRAND);

        // Get all the productList where brand equals to UPDATED_BRAND
        defaultProductShouldNotBeFound("brand.in=" + UPDATED_BRAND);
    }

    @Test
    @Transactional
    public void getAllProductsByBrandIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where brand is not null
        defaultProductShouldBeFound("brand.specified=true");

        // Get all the productList where brand is null
        defaultProductShouldNotBeFound("brand.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByBrandContainsSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where brand contains DEFAULT_BRAND
        defaultProductShouldBeFound("brand.contains=" + DEFAULT_BRAND);

        // Get all the productList where brand contains UPDATED_BRAND
        defaultProductShouldNotBeFound("brand.contains=" + UPDATED_BRAND);
    }

    @Test
    @Transactional
    public void getAllProductsByBrandNotContainsSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where brand does not contain DEFAULT_BRAND
        defaultProductShouldNotBeFound("brand.doesNotContain=" + DEFAULT_BRAND);

        // Get all the productList where brand does not contain UPDATED_BRAND
        defaultProductShouldBeFound("brand.doesNotContain=" + UPDATED_BRAND);
    }


    @Test
    @Transactional
    public void getAllProductsByCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where category equals to DEFAULT_CATEGORY
        defaultProductShouldBeFound("category.equals=" + DEFAULT_CATEGORY);

        // Get all the productList where category equals to UPDATED_CATEGORY
        defaultProductShouldNotBeFound("category.equals=" + UPDATED_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllProductsByCategoryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where category not equals to DEFAULT_CATEGORY
        defaultProductShouldNotBeFound("category.notEquals=" + DEFAULT_CATEGORY);

        // Get all the productList where category not equals to UPDATED_CATEGORY
        defaultProductShouldBeFound("category.notEquals=" + UPDATED_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllProductsByCategoryIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where category in DEFAULT_CATEGORY or UPDATED_CATEGORY
        defaultProductShouldBeFound("category.in=" + DEFAULT_CATEGORY + "," + UPDATED_CATEGORY);

        // Get all the productList where category equals to UPDATED_CATEGORY
        defaultProductShouldNotBeFound("category.in=" + UPDATED_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllProductsByCategoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where category is not null
        defaultProductShouldBeFound("category.specified=true");

        // Get all the productList where category is null
        defaultProductShouldNotBeFound("category.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsBySaleIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where sale equals to DEFAULT_SALE
        defaultProductShouldBeFound("sale.equals=" + DEFAULT_SALE);

        // Get all the productList where sale equals to UPDATED_SALE
        defaultProductShouldNotBeFound("sale.equals=" + UPDATED_SALE);
    }

    @Test
    @Transactional
    public void getAllProductsBySaleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where sale not equals to DEFAULT_SALE
        defaultProductShouldNotBeFound("sale.notEquals=" + DEFAULT_SALE);

        // Get all the productList where sale not equals to UPDATED_SALE
        defaultProductShouldBeFound("sale.notEquals=" + UPDATED_SALE);
    }

    @Test
    @Transactional
    public void getAllProductsBySaleIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where sale in DEFAULT_SALE or UPDATED_SALE
        defaultProductShouldBeFound("sale.in=" + DEFAULT_SALE + "," + UPDATED_SALE);

        // Get all the productList where sale equals to UPDATED_SALE
        defaultProductShouldNotBeFound("sale.in=" + UPDATED_SALE);
    }

    @Test
    @Transactional
    public void getAllProductsBySaleIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where sale is not null
        defaultProductShouldBeFound("sale.specified=true");

        // Get all the productList where sale is null
        defaultProductShouldNotBeFound("sale.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where tags equals to DEFAULT_TAGS
        defaultProductShouldBeFound("tags.equals=" + DEFAULT_TAGS);

        // Get all the productList where tags equals to UPDATED_TAGS
        defaultProductShouldNotBeFound("tags.equals=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllProductsByTagsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where tags not equals to DEFAULT_TAGS
        defaultProductShouldNotBeFound("tags.notEquals=" + DEFAULT_TAGS);

        // Get all the productList where tags not equals to UPDATED_TAGS
        defaultProductShouldBeFound("tags.notEquals=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllProductsByTagsIsInShouldWork() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where tags in DEFAULT_TAGS or UPDATED_TAGS
        defaultProductShouldBeFound("tags.in=" + DEFAULT_TAGS + "," + UPDATED_TAGS);

        // Get all the productList where tags equals to UPDATED_TAGS
        defaultProductShouldNotBeFound("tags.in=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllProductsByTagsIsNullOrNotNull() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        // Get all the productList where tags is not null
        defaultProductShouldBeFound("tags.specified=true");

        // Get all the productList where tags is null
        defaultProductShouldNotBeFound("tags.specified=false");
    }

    @Test
    @Transactional
    public void getAllProductsByCompanyIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        Company company = CompanyResourceIT.createEntity(em);
        em.persist(company);
        em.flush();
        product.setCompany(company);
        productRepository.saveAndFlush(product);
        Long companyId = company.getId();

        // Get all the productList where company equals to companyId
        defaultProductShouldBeFound("companyId.equals=" + companyId);

        // Get all the productList where company equals to companyId + 1
        defaultProductShouldNotBeFound("companyId.equals=" + (companyId + 1));
    }


    @Test
    @Transactional
    public void getAllProductsByImageIsEqualToSomething() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);
        Image image = ImageResourceIT.createEntity(em);
        em.persist(image);
        em.flush();
        product.addImage(image);
        productRepository.saveAndFlush(product);
        Long imageId = image.getId();

        // Get all the productList where image equals to imageId
        defaultProductShouldBeFound("imageId.equals=" + imageId);

        // Get all the productList where image equals to imageId + 1
        defaultProductShouldNotBeFound("imageId.equals=" + (imageId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProductShouldBeFound(String filter) throws Exception {
        restProductMockMvc.perform(get("/api/products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(product.getId().intValue())))
            .andExpect(jsonPath("$.[*].productName").value(hasItem(DEFAULT_PRODUCT_NAME)))
            .andExpect(jsonPath("$.[*].productCode").value(hasItem(DEFAULT_PRODUCT_CODE.intValue())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].stock").value(hasItem(DEFAULT_STOCK.intValue())))
            .andExpect(jsonPath("$.[*].isNew").value(hasItem(DEFAULT_IS_NEW.booleanValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].brand").value(hasItem(DEFAULT_BRAND)))
            .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].sale").value(hasItem(DEFAULT_SALE.booleanValue())))
            .andExpect(jsonPath("$.[*].tags").value(hasItem(DEFAULT_TAGS.toString())));

        // Check, that the count call also returns 1
        restProductMockMvc.perform(get("/api/products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProductShouldNotBeFound(String filter) throws Exception {
        restProductMockMvc.perform(get("/api/products?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProductMockMvc.perform(get("/api/products/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProduct() throws Exception {
        // Get the product
        restProductMockMvc.perform(get("/api/products/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = {AuthoritiesConstants.COMPANY})
    @Transactional
    public void updateProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        int databaseSizeBeforeUpdate = productRepository.findAll().size();

        // Update the product
        Product updatedProduct = productRepository.findById(product.getId()).get();
        // Disconnect from session so that the updates on updatedProduct are not directly saved in db
        em.detach(updatedProduct);
        updatedProduct
            .productName(UPDATED_PRODUCT_NAME)
            .productCode(UPDATED_PRODUCT_CODE)
            .price(UPDATED_PRICE)
            .discount(UPDATED_DISCOUNT)
            .currency(UPDATED_CURRENCY)
            .description(UPDATED_DESCRIPTION)
            .stock(UPDATED_STOCK)
            .isNew(UPDATED_IS_NEW)
            .type(UPDATED_TYPE)
            .brand(UPDATED_BRAND)
            .category(UPDATED_CATEGORY)
            .sale(UPDATED_SALE)
            .tags(UPDATED_TAGS);
        ProductDTO productDTO = productMapper.toDto(updatedProduct);

        restProductMockMvc.perform(put("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isOk());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeUpdate);
        Product testProduct = productList.get(productList.size() - 1);
        assertThat(testProduct.getProductName()).isEqualTo(UPDATED_PRODUCT_NAME);
        assertThat(testProduct.getProductCode()).isEqualTo(UPDATED_PRODUCT_CODE);
        assertThat(testProduct.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testProduct.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testProduct.getCurrency()).isEqualTo(UPDATED_CURRENCY);
        assertThat(testProduct.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProduct.getStock()).isEqualTo(UPDATED_STOCK);
        assertThat(testProduct.isIsNew()).isEqualTo(UPDATED_IS_NEW);
        assertThat(testProduct.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testProduct.getBrand()).isEqualTo(UPDATED_BRAND);
        assertThat(testProduct.getCategory()).isEqualTo(UPDATED_CATEGORY);
        assertThat(testProduct.isSale()).isEqualTo(UPDATED_SALE);
        assertThat(testProduct.getTags()).isEqualTo(UPDATED_TAGS);
    }

    @Test
    @WithMockUser(authorities = {AuthoritiesConstants.COMPANY})
    @Transactional
    public void updateNonExistingProduct() throws Exception {
        int databaseSizeBeforeUpdate = productRepository.findAll().size();

        // Create the Product
        ProductDTO productDTO = productMapper.toDto(product);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductMockMvc.perform(put("/api/products")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Product in the database
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProduct() throws Exception {
        // Initialize the database
        productRepository.saveAndFlush(product);

        int databaseSizeBeforeDelete = productRepository.findAll().size();

        // Delete the product
        restProductMockMvc.perform(delete("/api/products/{id}", product.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Product> productList = productRepository.findAll();
        assertThat(productList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
