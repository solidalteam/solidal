package it.albertodaleo.app.web.rest;

import it.albertodaleo.app.SolidalApp;
import it.albertodaleo.app.domain.Image;
import it.albertodaleo.app.domain.Product;
import it.albertodaleo.app.repository.ImageRepository;
import it.albertodaleo.app.service.ImageService;
import it.albertodaleo.app.service.dto.ImageDTO;
import it.albertodaleo.app.service.mapper.ImageMapper;
import it.albertodaleo.app.service.dto.ImageCriteria;
import it.albertodaleo.app.service.ImageQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ImageResource} REST controller.
 */
@SpringBootTest(classes = SolidalApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class ImageResourceIT {

    private static final Long DEFAULT_IMAGE_ID = 1L;
    private static final Long UPDATED_IMAGE_ID = 2L;
    private static final Long SMALLER_IMAGE_ID = 1L - 1L;

    private static final String DEFAULT_ALT = "AAAAAAAAAA";
    private static final String UPDATED_ALT = "BBBBBBBBBB";

    private static final String DEFAULT_SRC = "AAAAAAAAAA";
    private static final String UPDATED_SRC = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ImageMapper imageMapper;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ImageQueryService imageQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restImageMockMvc;

    private Image image;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Image createEntity(EntityManager em) {
        Image image = new Image()
            .imageId(DEFAULT_IMAGE_ID)
            .alt(DEFAULT_ALT)
            .src(DEFAULT_SRC)
            .description(DEFAULT_DESCRIPTION);
        return image;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Image createUpdatedEntity(EntityManager em) {
        Image image = new Image()
            .imageId(UPDATED_IMAGE_ID)
            .alt(UPDATED_ALT)
            .src(UPDATED_SRC)
            .description(UPDATED_DESCRIPTION);
        return image;
    }

    @BeforeEach
    public void initTest() {
        image = createEntity(em);
    }

    @Test
    @Transactional
    public void createImage() throws Exception {
        int databaseSizeBeforeCreate = imageRepository.findAll().size();

        // Create the Image
        ImageDTO imageDTO = imageMapper.toDto(image);
        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isCreated());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeCreate + 1);
        Image testImage = imageList.get(imageList.size() - 1);
        assertThat(testImage.getImageId()).isEqualTo(DEFAULT_IMAGE_ID);
        assertThat(testImage.getAlt()).isEqualTo(DEFAULT_ALT);
        assertThat(testImage.getSrc()).isEqualTo(DEFAULT_SRC);
        assertThat(testImage.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createImageWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = imageRepository.findAll().size();

        // Create the Image with an existing ID
        image.setId(1L);
        ImageDTO imageDTO = imageMapper.toDto(image);

        // An entity with an existing ID cannot be created, so this API call must fail
        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkImageIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = imageRepository.findAll().size();
        // set the field null
        image.setImageId(null);

        // Create the Image, which fails.
        ImageDTO imageDTO = imageMapper.toDto(image);

        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSrcIsRequired() throws Exception {
        int databaseSizeBeforeTest = imageRepository.findAll().size();
        // set the field null
        image.setSrc(null);

        // Create the Image, which fails.
        ImageDTO imageDTO = imageMapper.toDto(image);

        restImageMockMvc.perform(post("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllImages() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList
        restImageMockMvc.perform(get("/api/images?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(image.getId().intValue())))
            .andExpect(jsonPath("$.[*].imageId").value(hasItem(DEFAULT_IMAGE_ID.intValue())))
            .andExpect(jsonPath("$.[*].alt").value(hasItem(DEFAULT_ALT)))
            .andExpect(jsonPath("$.[*].src").value(hasItem(DEFAULT_SRC)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get the image
        restImageMockMvc.perform(get("/api/images/{id}", image.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(image.getId().intValue()))
            .andExpect(jsonPath("$.imageId").value(DEFAULT_IMAGE_ID.intValue()))
            .andExpect(jsonPath("$.alt").value(DEFAULT_ALT))
            .andExpect(jsonPath("$.src").value(DEFAULT_SRC))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }


    @Test
    @Transactional
    public void getImagesByIdFiltering() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        Long id = image.getId();

        defaultImageShouldBeFound("id.equals=" + id);
        defaultImageShouldNotBeFound("id.notEquals=" + id);

        defaultImageShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultImageShouldNotBeFound("id.greaterThan=" + id);

        defaultImageShouldBeFound("id.lessThanOrEqual=" + id);
        defaultImageShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllImagesByImageIdIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId equals to DEFAULT_IMAGE_ID
        defaultImageShouldBeFound("imageId.equals=" + DEFAULT_IMAGE_ID);

        // Get all the imageList where imageId equals to UPDATED_IMAGE_ID
        defaultImageShouldNotBeFound("imageId.equals=" + UPDATED_IMAGE_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByImageIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId not equals to DEFAULT_IMAGE_ID
        defaultImageShouldNotBeFound("imageId.notEquals=" + DEFAULT_IMAGE_ID);

        // Get all the imageList where imageId not equals to UPDATED_IMAGE_ID
        defaultImageShouldBeFound("imageId.notEquals=" + UPDATED_IMAGE_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByImageIdIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId in DEFAULT_IMAGE_ID or UPDATED_IMAGE_ID
        defaultImageShouldBeFound("imageId.in=" + DEFAULT_IMAGE_ID + "," + UPDATED_IMAGE_ID);

        // Get all the imageList where imageId equals to UPDATED_IMAGE_ID
        defaultImageShouldNotBeFound("imageId.in=" + UPDATED_IMAGE_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByImageIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId is not null
        defaultImageShouldBeFound("imageId.specified=true");

        // Get all the imageList where imageId is null
        defaultImageShouldNotBeFound("imageId.specified=false");
    }

    @Test
    @Transactional
    public void getAllImagesByImageIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId is greater than or equal to DEFAULT_IMAGE_ID
        defaultImageShouldBeFound("imageId.greaterThanOrEqual=" + DEFAULT_IMAGE_ID);

        // Get all the imageList where imageId is greater than or equal to UPDATED_IMAGE_ID
        defaultImageShouldNotBeFound("imageId.greaterThanOrEqual=" + UPDATED_IMAGE_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByImageIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId is less than or equal to DEFAULT_IMAGE_ID
        defaultImageShouldBeFound("imageId.lessThanOrEqual=" + DEFAULT_IMAGE_ID);

        // Get all the imageList where imageId is less than or equal to SMALLER_IMAGE_ID
        defaultImageShouldNotBeFound("imageId.lessThanOrEqual=" + SMALLER_IMAGE_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByImageIdIsLessThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId is less than DEFAULT_IMAGE_ID
        defaultImageShouldNotBeFound("imageId.lessThan=" + DEFAULT_IMAGE_ID);

        // Get all the imageList where imageId is less than UPDATED_IMAGE_ID
        defaultImageShouldBeFound("imageId.lessThan=" + UPDATED_IMAGE_ID);
    }

    @Test
    @Transactional
    public void getAllImagesByImageIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where imageId is greater than DEFAULT_IMAGE_ID
        defaultImageShouldNotBeFound("imageId.greaterThan=" + DEFAULT_IMAGE_ID);

        // Get all the imageList where imageId is greater than SMALLER_IMAGE_ID
        defaultImageShouldBeFound("imageId.greaterThan=" + SMALLER_IMAGE_ID);
    }


    @Test
    @Transactional
    public void getAllImagesByAltIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where alt equals to DEFAULT_ALT
        defaultImageShouldBeFound("alt.equals=" + DEFAULT_ALT);

        // Get all the imageList where alt equals to UPDATED_ALT
        defaultImageShouldNotBeFound("alt.equals=" + UPDATED_ALT);
    }

    @Test
    @Transactional
    public void getAllImagesByAltIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where alt not equals to DEFAULT_ALT
        defaultImageShouldNotBeFound("alt.notEquals=" + DEFAULT_ALT);

        // Get all the imageList where alt not equals to UPDATED_ALT
        defaultImageShouldBeFound("alt.notEquals=" + UPDATED_ALT);
    }

    @Test
    @Transactional
    public void getAllImagesByAltIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where alt in DEFAULT_ALT or UPDATED_ALT
        defaultImageShouldBeFound("alt.in=" + DEFAULT_ALT + "," + UPDATED_ALT);

        // Get all the imageList where alt equals to UPDATED_ALT
        defaultImageShouldNotBeFound("alt.in=" + UPDATED_ALT);
    }

    @Test
    @Transactional
    public void getAllImagesByAltIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where alt is not null
        defaultImageShouldBeFound("alt.specified=true");

        // Get all the imageList where alt is null
        defaultImageShouldNotBeFound("alt.specified=false");
    }
                @Test
    @Transactional
    public void getAllImagesByAltContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where alt contains DEFAULT_ALT
        defaultImageShouldBeFound("alt.contains=" + DEFAULT_ALT);

        // Get all the imageList where alt contains UPDATED_ALT
        defaultImageShouldNotBeFound("alt.contains=" + UPDATED_ALT);
    }

    @Test
    @Transactional
    public void getAllImagesByAltNotContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where alt does not contain DEFAULT_ALT
        defaultImageShouldNotBeFound("alt.doesNotContain=" + DEFAULT_ALT);

        // Get all the imageList where alt does not contain UPDATED_ALT
        defaultImageShouldBeFound("alt.doesNotContain=" + UPDATED_ALT);
    }


    @Test
    @Transactional
    public void getAllImagesBySrcIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where src equals to DEFAULT_SRC
        defaultImageShouldBeFound("src.equals=" + DEFAULT_SRC);

        // Get all the imageList where src equals to UPDATED_SRC
        defaultImageShouldNotBeFound("src.equals=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllImagesBySrcIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where src not equals to DEFAULT_SRC
        defaultImageShouldNotBeFound("src.notEquals=" + DEFAULT_SRC);

        // Get all the imageList where src not equals to UPDATED_SRC
        defaultImageShouldBeFound("src.notEquals=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllImagesBySrcIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where src in DEFAULT_SRC or UPDATED_SRC
        defaultImageShouldBeFound("src.in=" + DEFAULT_SRC + "," + UPDATED_SRC);

        // Get all the imageList where src equals to UPDATED_SRC
        defaultImageShouldNotBeFound("src.in=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllImagesBySrcIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where src is not null
        defaultImageShouldBeFound("src.specified=true");

        // Get all the imageList where src is null
        defaultImageShouldNotBeFound("src.specified=false");
    }
                @Test
    @Transactional
    public void getAllImagesBySrcContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where src contains DEFAULT_SRC
        defaultImageShouldBeFound("src.contains=" + DEFAULT_SRC);

        // Get all the imageList where src contains UPDATED_SRC
        defaultImageShouldNotBeFound("src.contains=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllImagesBySrcNotContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where src does not contain DEFAULT_SRC
        defaultImageShouldNotBeFound("src.doesNotContain=" + DEFAULT_SRC);

        // Get all the imageList where src does not contain UPDATED_SRC
        defaultImageShouldBeFound("src.doesNotContain=" + UPDATED_SRC);
    }


    @Test
    @Transactional
    public void getAllImagesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where description equals to DEFAULT_DESCRIPTION
        defaultImageShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the imageList where description equals to UPDATED_DESCRIPTION
        defaultImageShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllImagesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where description not equals to DEFAULT_DESCRIPTION
        defaultImageShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the imageList where description not equals to UPDATED_DESCRIPTION
        defaultImageShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllImagesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultImageShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the imageList where description equals to UPDATED_DESCRIPTION
        defaultImageShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllImagesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where description is not null
        defaultImageShouldBeFound("description.specified=true");

        // Get all the imageList where description is null
        defaultImageShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllImagesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where description contains DEFAULT_DESCRIPTION
        defaultImageShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the imageList where description contains UPDATED_DESCRIPTION
        defaultImageShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllImagesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        // Get all the imageList where description does not contain DEFAULT_DESCRIPTION
        defaultImageShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the imageList where description does not contain UPDATED_DESCRIPTION
        defaultImageShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllImagesByProductIsEqualToSomething() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);
        Product product = ProductResourceIT.createEntity(em);
        em.persist(product);
        em.flush();
        image.setProduct(product);
        imageRepository.saveAndFlush(image);
        Long productId = product.getId();

        // Get all the imageList where product equals to productId
        defaultImageShouldBeFound("productId.equals=" + productId);

        // Get all the imageList where product equals to productId + 1
        defaultImageShouldNotBeFound("productId.equals=" + (productId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultImageShouldBeFound(String filter) throws Exception {
        restImageMockMvc.perform(get("/api/images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(image.getId().intValue())))
            .andExpect(jsonPath("$.[*].imageId").value(hasItem(DEFAULT_IMAGE_ID.intValue())))
            .andExpect(jsonPath("$.[*].alt").value(hasItem(DEFAULT_ALT)))
            .andExpect(jsonPath("$.[*].src").value(hasItem(DEFAULT_SRC)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));

        // Check, that the count call also returns 1
        restImageMockMvc.perform(get("/api/images/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultImageShouldNotBeFound(String filter) throws Exception {
        restImageMockMvc.perform(get("/api/images?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restImageMockMvc.perform(get("/api/images/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingImage() throws Exception {
        // Get the image
        restImageMockMvc.perform(get("/api/images/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        int databaseSizeBeforeUpdate = imageRepository.findAll().size();

        // Update the image
        Image updatedImage = imageRepository.findById(image.getId()).get();
        // Disconnect from session so that the updates on updatedImage are not directly saved in db
        em.detach(updatedImage);
        updatedImage
            .imageId(UPDATED_IMAGE_ID)
            .alt(UPDATED_ALT)
            .src(UPDATED_SRC)
            .description(UPDATED_DESCRIPTION);
        ImageDTO imageDTO = imageMapper.toDto(updatedImage);

        restImageMockMvc.perform(put("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isOk());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeUpdate);
        Image testImage = imageList.get(imageList.size() - 1);
        assertThat(testImage.getImageId()).isEqualTo(UPDATED_IMAGE_ID);
        assertThat(testImage.getAlt()).isEqualTo(UPDATED_ALT);
        assertThat(testImage.getSrc()).isEqualTo(UPDATED_SRC);
        assertThat(testImage.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingImage() throws Exception {
        int databaseSizeBeforeUpdate = imageRepository.findAll().size();

        // Create the Image
        ImageDTO imageDTO = imageMapper.toDto(image);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restImageMockMvc.perform(put("/api/images")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(imageDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Image in the database
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteImage() throws Exception {
        // Initialize the database
        imageRepository.saveAndFlush(image);

        int databaseSizeBeforeDelete = imageRepository.findAll().size();

        // Delete the image
        restImageMockMvc.perform(delete("/api/images/{id}", image.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Image> imageList = imageRepository.findAll();
        assertThat(imageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
