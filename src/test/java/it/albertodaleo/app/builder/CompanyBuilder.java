package it.albertodaleo.app.builder;

import it.albertodaleo.app.domain.Company;
import it.albertodaleo.app.domain.User;

import java.time.LocalDate;

public final class CompanyBuilder {
    private Long id;
    private String name;
    private String vatNumber;
    private LocalDate creationDate;
    private String address1;
    private String address2;
    private String telephoneNumber;
    private String ownerName;
    private String country;
    private String postalCode;
    private String province;
    private User user;

    private CompanyBuilder() {
    }

    public static CompanyBuilder aCompany() {
        return new CompanyBuilder();
    }

    public static CompanyBuilder aDefaultCompany() {
        CompanyBuilder companyBuilder = new CompanyBuilder();
        companyBuilder.name = "aCompany";
        companyBuilder.vatNumber = "aVatNumber";
        companyBuilder.creationDate = LocalDate.of(2020, 12, 12);
        companyBuilder.address1 = "aAddress1";
        companyBuilder.address2 = "aAddress2";
        companyBuilder.telephoneNumber = "aTelephoneNumber";
        companyBuilder.ownerName = "aOwnerName";
        companyBuilder.country = "aCountry";
        companyBuilder.postalCode = "aPostalCode";
        companyBuilder.province = "MI";

        return companyBuilder;
    }

    public CompanyBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public CompanyBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public CompanyBuilder withVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
        return this;
    }

    public CompanyBuilder withCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public CompanyBuilder withAddress1(String address1) {
        this.address1 = address1;
        return this;
    }

    public CompanyBuilder withAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    public CompanyBuilder withTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public CompanyBuilder withOwnerName(String ownerName) {
        this.ownerName = ownerName;
        return this;
    }

    public CompanyBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public CompanyBuilder withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public CompanyBuilder withProvince(String province) {
        this.province = province;
        return this;
    }

    public CompanyBuilder withUser(User user) {
        this.user = user;
        return this;
    }

    public Company build() {
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setVatNumber(vatNumber);
        company.setCreationDate(creationDate);
        company.setAddress1(address1);
        company.setAddress2(address2);
        company.setTelephoneNumber(telephoneNumber);
        company.setOwnerName(ownerName);
        company.setCountry(country);
        company.setPostalCode(postalCode);
        company.setProvince(province);
        company.setUser(user);
        return company;
    }
}
