package it.albertodaleo.app.builder;

import it.albertodaleo.app.service.dto.CompanyDTO;

import java.time.LocalDate;

public final class CompanyDTOBuilder {
    private Long id;
    private String name;
    private String vatNumber;
    private LocalDate creationDate;
    private String address1;
    private String address2;
    private String telephoneNumber;
    private String ownerName;
    private String country;
    private String postalCode;
    private String province;

    private CompanyDTOBuilder() {
    }

    public static CompanyDTOBuilder aCompanyDTO() {
        return new CompanyDTOBuilder();
    }

    public static CompanyDTOBuilder aDefaultCompanyDTO() {
        CompanyDTOBuilder companyDTOBuilder = new CompanyDTOBuilder();

        companyDTOBuilder.name = "aCompany";
        companyDTOBuilder.vatNumber = "aVatNumber";
        companyDTOBuilder.creationDate = LocalDate.of(2020, 12, 12);
        companyDTOBuilder.address1 = "aAddress1";
        companyDTOBuilder.address2 = "aAddress2";
        companyDTOBuilder.telephoneNumber = "aTelephoneNumber";
        companyDTOBuilder.ownerName = "aOwnerName";
        companyDTOBuilder.country = "aCountry";
        companyDTOBuilder.postalCode = "aPostalCode";
        companyDTOBuilder.province = "MI";

        return companyDTOBuilder;
    }

    public CompanyDTOBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public CompanyDTOBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public CompanyDTOBuilder withVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
        return this;
    }

    public CompanyDTOBuilder withCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public CompanyDTOBuilder withAddress1(String address1) {
        this.address1 = address1;
        return this;
    }

    public CompanyDTOBuilder withAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    public CompanyDTOBuilder withTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public CompanyDTOBuilder withOwnerName(String ownerName) {
        this.ownerName = ownerName;
        return this;
    }

    public CompanyDTOBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public CompanyDTOBuilder withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public CompanyDTOBuilder withProvince(String province) {
        this.province = province;
        return this;
    }

    public CompanyDTO build() {
        CompanyDTO companyDTO = new CompanyDTO();
        companyDTO.setId(id);
        companyDTO.setName(name);
        companyDTO.setVatNumber(vatNumber);
        companyDTO.setCreationDate(creationDate);
        companyDTO.setAddress1(address1);
        companyDTO.setAddress2(address2);
        companyDTO.setTelephoneNumber(telephoneNumber);
        companyDTO.setOwnerName(ownerName);
        companyDTO.setCountry(country);
        companyDTO.setPostalCode(postalCode);
        companyDTO.setProvince(province);
        return companyDTO;
    }
}
