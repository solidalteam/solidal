package it.albertodaleo.app.component;

import com.google.maps.errors.ApiException;
import it.albertodaleo.app.SolidalApp;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Optional;

@SpringBootTest(classes = SolidalApp.class)
public class ReverseGeocodingComponentTest {

    @InjectMocks
    ReverseGeocodingComponent target;

    @Test
    void shouldConvertCoordinatesToPostalCode() throws InterruptedException, ApiException, IOException {
        // GIVEN

        // WHEN
        Optional<String> result = target.reverseGeocoding(45.505812, 9.200474);

        // THEN
        Assertions.assertThat(result.get()).isEqualTo("MI");
    }
}
