package it.albertodaleo.app.domain;

import it.albertodaleo.app.domain.enumeration.Category;
import it.albertodaleo.app.domain.enumeration.Currency;
import it.albertodaleo.app.domain.enumeration.Tags;
import it.albertodaleo.app.domain.enumeration.Type;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public final class ProductBuilder {
    private Long id;
    private String productName;
    private BigDecimal productCode;
    private BigDecimal price;
    private Double discount;
    private Currency currency;
    private String description;
    private Long stock;
    private Boolean isNew;
    private Type type;
    private String brand;
    private Category category;
    private Boolean sale;
    private Tags tags;
    private Company company;
    private Set<Image> images = new HashSet<>();

    private ProductBuilder() {
    }

    public static ProductBuilder aProduct() {
        return new ProductBuilder();
    }

    public ProductBuilder withId(Long id) {
        this.id = id;
        return this;
    }

    public ProductBuilder withProductName(String productName) {
        this.productName = productName;
        return this;
    }

    public ProductBuilder withProductCode(BigDecimal productCode) {
        this.productCode = productCode;
        return this;
    }

    public ProductBuilder withPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public ProductBuilder withDiscount(Double discount) {
        this.discount = discount;
        return this;
    }

    public ProductBuilder withCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public ProductBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public ProductBuilder withStock(Long stock) {
        this.stock = stock;
        return this;
    }

    public ProductBuilder withIsNew(Boolean isNew) {
        this.isNew = isNew;
        return this;
    }

    public ProductBuilder withType(Type type) {
        this.type = type;
        return this;
    }

    public ProductBuilder withBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public ProductBuilder withCategory(Category category) {
        this.category = category;
        return this;
    }

    public ProductBuilder withSale(Boolean sale) {
        this.sale = sale;
        return this;
    }

    public ProductBuilder withTags(Tags tags) {
        this.tags = tags;
        return this;
    }

    public ProductBuilder withCompany(Company company) {
        this.company = company;
        return this;
    }

    public ProductBuilder withImages(Set<Image> images) {
        this.images = images;
        return this;
    }

    public Product build() {
        Product product = new Product();
        product.setId(id);
        product.setProductName(productName);
        product.setProductCode(productCode);
        product.setPrice(price);
        product.setDiscount(discount);
        product.setCurrency(currency);
        product.setDescription(description);
        product.setStock(stock);
        product.setIsNew(isNew);
        product.setType(type);
        product.setBrand(brand);
        product.setCategory(category);
        product.setSale(sale);
        product.setTags(tags);
        product.setCompany(company);
        product.setImages(images);
        return product;
    }
}
