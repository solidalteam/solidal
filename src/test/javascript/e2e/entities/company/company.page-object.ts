import { element, by, ElementFinder } from 'protractor';

export class CompanyComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-company div table .btn-danger'));
  title = element.all(by.css('jhi-company div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CompanyUpdatePage {
  pageTitle = element(by.id('jhi-company-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nameInput = element(by.id('field_name'));
  vatNumberInput = element(by.id('field_vatNumber'));
  creationDateInput = element(by.id('field_creationDate'));
  address1Input = element(by.id('field_address1'));
  address2Input = element(by.id('field_address2'));
  telephoneNumberInput = element(by.id('field_telephoneNumber'));
  ownerNameInput = element(by.id('field_ownerName'));
  countryInput = element(by.id('field_country'));
  postalCodeInput = element(by.id('field_postalCode'));
  provinceInput = element(by.id('field_province'));

  userSelect = element(by.id('field_user'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setVatNumberInput(vatNumber: string): Promise<void> {
    await this.vatNumberInput.sendKeys(vatNumber);
  }

  async getVatNumberInput(): Promise<string> {
    return await this.vatNumberInput.getAttribute('value');
  }

  async setCreationDateInput(creationDate: string): Promise<void> {
    await this.creationDateInput.sendKeys(creationDate);
  }

  async getCreationDateInput(): Promise<string> {
    return await this.creationDateInput.getAttribute('value');
  }

  async setAddress1Input(address1: string): Promise<void> {
    await this.address1Input.sendKeys(address1);
  }

  async getAddress1Input(): Promise<string> {
    return await this.address1Input.getAttribute('value');
  }

  async setAddress2Input(address2: string): Promise<void> {
    await this.address2Input.sendKeys(address2);
  }

  async getAddress2Input(): Promise<string> {
    return await this.address2Input.getAttribute('value');
  }

  async setTelephoneNumberInput(telephoneNumber: string): Promise<void> {
    await this.telephoneNumberInput.sendKeys(telephoneNumber);
  }

  async getTelephoneNumberInput(): Promise<string> {
    return await this.telephoneNumberInput.getAttribute('value');
  }

  async setOwnerNameInput(ownerName: string): Promise<void> {
    await this.ownerNameInput.sendKeys(ownerName);
  }

  async getOwnerNameInput(): Promise<string> {
    return await this.ownerNameInput.getAttribute('value');
  }

  async setCountryInput(country: string): Promise<void> {
    await this.countryInput.sendKeys(country);
  }

  async getCountryInput(): Promise<string> {
    return await this.countryInput.getAttribute('value');
  }

  async setPostalCodeInput(postalCode: string): Promise<void> {
    await this.postalCodeInput.sendKeys(postalCode);
  }

  async getPostalCodeInput(): Promise<string> {
    return await this.postalCodeInput.getAttribute('value');
  }

  async setProvinceInput(province: string): Promise<void> {
    await this.provinceInput.sendKeys(province);
  }

  async getProvinceInput(): Promise<string> {
    return await this.provinceInput.getAttribute('value');
  }

  async userSelectLastOption(): Promise<void> {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option: string): Promise<void> {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption(): Promise<string> {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CompanyDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-company-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-company'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
