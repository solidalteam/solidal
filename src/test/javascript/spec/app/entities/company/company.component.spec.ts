import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { SolidalTestModule } from '../../../test.module';
import { CompanyComponent } from 'app/entities/company/company.component';
import { CompanyService } from 'app/entities/company/company.service';
import { Company } from 'app/shared/model/company.model';

describe('Component Tests', () => {
  describe('Company Management Component', () => {
    let target: CompanyComponent;
    let fixture: ComponentFixture<CompanyComponent>;
    let service: CompanyService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SolidalTestModule],
        declarations: [CompanyComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(CompanyComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CompanyComponent);
      target = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CompanyService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'getOwnedCompanies').and.returnValue(
        of(
          new HttpResponse({
            body: [new Company(123)],
            headers
          })
        )
      );

      // WHEN
      target.ngOnInit();

      // THEN
      expect(service.getOwnedCompanies).toHaveBeenCalled();
      expect(target.companies && target.companies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'getOwnedCompanies').and.returnValue(
        of(
          new HttpResponse({
            body: [new Company(123)],
            headers
          })
        )
      );

      // WHEN
      target.loadPage(1);

      // THEN
      expect(service.getOwnedCompanies).toHaveBeenCalled();
      expect(target.companies && target.companies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should re-initialize the page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'getOwnedCompanies').and.returnValue(
        of(
          new HttpResponse({
            body: [new Company(123)],
            headers
          })
        )
      );

      // WHEN
      target.loadPage(1);
      target.reset();

      // THEN
      expect(target.page).toEqual(0);
      expect(service.getOwnedCompanies).toHaveBeenCalledTimes(2);
      expect(target.companies && target.companies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      target.ngOnInit();
      const result = target.sort();

      // THEN
      expect(result).toEqual(['id,asc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      target.ngOnInit();

      // GIVEN
      target.predicate = 'name';

      // WHEN
      const result = target.sort();

      // THEN
      expect(result).toEqual(['name,asc', 'id']);
    });
  });
});
