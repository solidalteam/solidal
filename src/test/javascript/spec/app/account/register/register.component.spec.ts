import { async, ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { JhiLanguageService } from 'ng-jhipster';

import { MockLanguageService } from '../../../helpers/mock-language.service';
import { SolidalTestModule } from '../../../test.module';
import { EMAIL_ALREADY_USED_TYPE, LOGIN_ALREADY_USED_TYPE } from 'app/shared/constants/error.constants';
import { RegisterService } from 'app/account/register/register.service';
import { RegisterComponent } from 'app/account/register/register.component';

describe('Component Tests', () => {
  describe('RegisterComponent', () => {
    let fixture: ComponentFixture<RegisterComponent>;
    let target: RegisterComponent;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [SolidalTestModule],
        declarations: [RegisterComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RegisterComponent, '')
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(RegisterComponent);
      target = fixture.componentInstance;
    });

    it('should ensure the two passwords entered match', () => {
      target.registerForm.patchValue({
        password: 'password',
        confirmPassword: 'non-matching'
      });

      target.register();

      expect(target.doNotMatch).toBe(true);
    });

    it('should update success to true after creating an account', inject(
      [RegisterService, JhiLanguageService],
      fakeAsync((service: RegisterService, mockTranslate: MockLanguageService) => {
        spyOn(service, 'save').and.returnValue(of({}));
        target.registerForm.patchValue({
          password: 'password',
          confirmPassword: 'password'
        });

        target.register();
        tick();

        expect(service.save).toHaveBeenCalledWith(
          {
            email: '',
            password: 'password',
            login: '',
            langKey: 'it'
          },
          false
        );
        expect(target.success).toBe(true);
        expect(mockTranslate.getCurrentLanguageSpy).toHaveBeenCalled();
        expect(target.errorUserExists).toBe(false);
        expect(target.errorEmailExists).toBe(false);
        expect(target.error).toBe(false);
      })
    ));

    it('should verify if user is a companyUser', inject(
      [RegisterService],
      fakeAsync((service: RegisterService) => {
        // GIVEN
        spyOn(service, 'save').and.returnValue(of({}));
        target.registerForm.get('isCompany')?.setValue(true);

        // WHEN
        target.register();
        tick();

        // THEN
        expect(service.save).toHaveBeenCalledWith(
          {
            email: '',
            password: '',
            login: '',
            langKey: 'it'
          },
          true
        );
      })
    ));

    it('should notify of user existence upon 400/login already in use', inject(
      [RegisterService],
      fakeAsync((service: RegisterService) => {
        spyOn(service, 'save').and.returnValue(
          throwError({
            status: 400,
            error: { type: LOGIN_ALREADY_USED_TYPE }
          })
        );
        target.registerForm.patchValue({
          password: 'password',
          confirmPassword: 'password'
        });

        target.register();
        tick();

        expect(target.errorUserExists).toBe(true);
        expect(target.errorEmailExists).toBe(false);
        expect(target.error).toBe(false);
      })
    ));

    it('should notify of email existence upon 400/email address already in use', inject(
      [RegisterService],
      fakeAsync((service: RegisterService) => {
        spyOn(service, 'save').and.returnValue(
          throwError({
            status: 400,
            error: { type: EMAIL_ALREADY_USED_TYPE }
          })
        );
        target.registerForm.patchValue({
          password: 'password',
          confirmPassword: 'password'
        });

        target.register();
        tick();

        expect(target.errorEmailExists).toBe(true);
        expect(target.errorUserExists).toBe(false);
        expect(target.error).toBe(false);
      })
    ));

    it('should notify of generic error', inject(
      [RegisterService],
      fakeAsync((service: RegisterService) => {
        spyOn(service, 'save').and.returnValue(
          throwError({
            status: 503
          })
        );
        target.registerForm.patchValue({
          password: 'password',
          confirmPassword: 'password'
        });

        target.register();
        tick();

        expect(target.errorUserExists).toBe(false);
        expect(target.errorEmailExists).toBe(false);
        expect(target.error).toBe(true);
      })
    ));
  });
});
