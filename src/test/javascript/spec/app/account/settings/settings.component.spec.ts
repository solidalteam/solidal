import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { of, throwError } from 'rxjs';

import { SolidalTestModule } from '../../../test.module';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { SettingsComponent } from 'app/account/settings/settings.component';
import { MockAccountService } from '../../../helpers/mock-account.service';
import { Company } from 'app/shared/model/company.model';
import * as moment from 'moment';

describe('Component Tests', () => {
  describe('SettingsComponent', () => {
    let target: SettingsComponent;
    let fixture: ComponentFixture<SettingsComponent>;
    let mockAuth: MockAccountService;
    const accountValues: Account = {
      firstName: 'John',
      lastName: 'Doe',
      activated: true,
      email: 'john.doe@mail.com',
      langKey: 'it',
      login: 'john',
      authorities: ['ROLE_USER'],
      imageUrl: ''
    };
    const creationDate = moment('12/12/2020');
    const company: Company = {
      name: 'azienda',
      vatNumber: '123',
      creationDate,
      address1: 'via romagna',
      telephoneNumber: '5524124251',
      ownerName: 'pippo',
      country: 'ITALY',
      postalCode: '20090',
      province: 'Milano'
    };
    const accountPlusCompanyValues: Account = {
      firstName: 'John',
      lastName: 'Doe',
      activated: true,
      email: 'john.doe@mail.com',
      langKey: 'it',
      login: 'john',
      authorities: ['ROLE_COMPANY', 'ROLE_USER'],
      imageUrl: ''
    };

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [SolidalTestModule],
        declarations: [SettingsComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(SettingsComponent, '')
        .compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(SettingsComponent);
      target = fixture.componentInstance;
      mockAuth = TestBed.get(AccountService);
    });

    it('should send the current identity upon save', () => {
      // GIVEN
      mockAuth.setIdentityResponse(accountValues);
      mockAuth.saveSpy.and.returnValue(of({}));
      const settingsFormValues = {
        firstName: 'John',
        lastName: 'Doe',
        email: 'john.doe@mail.com',
        langKey: 'it'
      };

      // WHEN
      target.ngOnInit();
      target.save();

      // THEN
      expect(mockAuth.identitySpy).toHaveBeenCalled();
      expect(mockAuth.saveSpy).toHaveBeenCalledWith(accountValues);
      expect(mockAuth.authenticateSpy).toHaveBeenCalledWith(accountValues);
      expect(target.settingsForm.value).toEqual(settingsFormValues);
    });

    it('should notify of success upon successful save', () => {
      // GIVEN
      mockAuth.setIdentityResponse(accountValues);
      mockAuth.saveSpy.and.returnValue(of({}));

      // WHEN
      target.ngOnInit();
      target.save();

      // THEN
      expect(target.success).toBe(true);
    });

    it('should notify of error upon failed save', () => {
      // GIVEN
      mockAuth.setIdentityResponse(accountValues);
      mockAuth.saveSpy.and.returnValue(throwError('ERROR'));

      // WHEN
      target.ngOnInit();
      target.save();

      // THEN
      expect(target.success).toBe(false);
    });
  });
});
