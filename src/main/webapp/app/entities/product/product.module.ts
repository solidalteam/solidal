import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SolidalSharedModule } from 'app/shared/shared.module';
import { ProductComponent } from './product.component';
import { ProductDetailComponent } from './product-detail.component';
import { ProductUpdateComponent } from './product-update.component';
import { ProductDeleteDialogComponent } from './product-delete-dialog.component';
import { productRoute } from './product.route';
import { CarouselModule } from 'ngx-owl-carousel-o';

@NgModule({
  imports: [SolidalSharedModule, RouterModule.forChild(productRoute), CarouselModule],
  declarations: [ProductComponent, ProductDetailComponent, ProductUpdateComponent, ProductDeleteDialogComponent],
  entryComponents: [ProductDeleteDialogComponent]
})
export class SolidalProductModule {}
