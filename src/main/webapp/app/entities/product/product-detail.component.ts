import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IProduct } from 'app/shared/model/product.model';
import { ProductDetailsMainSlider, ProductDetailsThumbSlider } from 'app/shared/data/slider';

@Component({
  selector: 'jhi-product-detail',
  templateUrl: './product-detail.component.html'
})
export class ProductDetailComponent implements OnInit {
  product: IProduct | null = null;
  counter = 1;
  activeSlide: any = 0;

  ProductDetailsMainSliderConfig: any = ProductDetailsMainSlider;
  ProductDetailsThumbConfig: any = ProductDetailsThumbSlider;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ product }) => (this.product = product));
  }

  previousState(): void {
    window.history.back();
  }

  increment(): void {
    this.counter++;
  }

  decrement(): void {
    if (this.counter > 1) this.counter--;
  }
}
