import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'company',
        loadChildren: () => import('./company/company.module').then(m => m.SolidalCompanyModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.SolidalProductModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class SolidalEntityModule {}
