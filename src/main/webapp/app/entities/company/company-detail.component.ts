import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICompany } from 'app/shared/model/company.model';
import { ProductDetailsMainSlider, ProductDetailsThumbSlider } from 'app/shared/data/slider';

@Component({
  selector: 'jhi-company-detail',
  templateUrl: './company-detail.component.html'
})
export class CompanyDetailComponent implements OnInit {
  company: ICompany | null = null;
  public counter = 1;
  public activeSlide: any = 0;

  public productDetailsMainSliderConfig: any = ProductDetailsMainSlider;
  public productDetailsThumbConfig: any = ProductDetailsThumbSlider;
  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ company }) => (this.company = company));
  }

  previousState(): void {
    window.history.back();
  }
}
