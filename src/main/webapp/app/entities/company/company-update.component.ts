import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICompany, Company } from 'app/shared/model/company.model';
import { CompanyService } from './company.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-company-update',
  templateUrl: './company-update.component.html'
})
export class CompanyUpdateComponent implements OnInit {
  isSaving = false;
  users: IUser[] = [];
  creationDateDp: any;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    vatNumber: [null, [Validators.required]],
    creationDate: [],
    address1: [null, [Validators.required]],
    address2: [],
    telephoneNumber: [],
    ownerName: [null, [Validators.required, Validators.minLength(3)]],
    country: [null, [Validators.required]],
    postalCode: [null, [Validators.required, Validators.minLength(5)]],
    province: [null, [Validators.required, Validators.maxLength(2)]],
    userId: []
  });

  constructor(
    protected companyService: CompanyService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ company }) => {
      this.updateForm(company);
    });
  }

  updateForm(company: ICompany): void {
    this.editForm.patchValue({
      id: company.id,
      name: company.name,
      vatNumber: company.vatNumber,
      creationDate: company.creationDate,
      address1: company.address1,
      address2: company.address2,
      telephoneNumber: company.telephoneNumber,
      ownerName: company.ownerName,
      country: company.country,
      postalCode: company.postalCode,
      province: company.province,
      userId: company.userId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const company = this.createFromForm();
    if (company.id !== undefined) {
      this.subscribeToSaveResponse(this.companyService.update(company));
    } else {
      this.subscribeToSaveResponse(this.companyService.create(company));
    }
  }

  private createFromForm(): ICompany {
    return {
      ...new Company(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      vatNumber: this.editForm.get(['vatNumber'])!.value,
      creationDate: this.editForm.get(['creationDate'])!.value,
      address1: this.editForm.get(['address1'])!.value,
      address2: this.editForm.get(['address2'])!.value,
      telephoneNumber: this.editForm.get(['telephoneNumber'])!.value,
      ownerName: this.editForm.get(['ownerName'])!.value,
      country: this.editForm.get(['country'])!.value,
      postalCode: this.editForm.get(['postalCode'])!.value,
      province: this.editForm.get(['province'])!.value,
      userId: this.editForm.get(['userId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICompany>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IUser): any {
    return item.id;
  }
}
