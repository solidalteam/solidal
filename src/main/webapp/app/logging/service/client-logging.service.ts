import { Injectable } from '@angular/core';
import { MessageToLog } from '../model/message-to-log.model';
import { LogFactory } from 'app/logging/factory/log-factory';
import { DataToLog } from 'app/logging/model/data-to-log.model';
import { DeviceDetectorService } from 'ngx-device-detector';

export enum LogType {
  DEBUG,
  TRACE,
  ERROR,
  INFO,
  WARN
}

@Injectable({
  providedIn: 'root'
})
export class ClientLoggingService {
  constructor(private logFactory: LogFactory, private deviceInfoService: DeviceDetectorService) {}

  sendInformation(type: LogType, error: MessageToLog): any {
    const dataEntry = this.createDataToLog(error);

    this.logFactory
      .getLog(type)
      .sendInformation(dataEntry)
      .subscribe();
  }

  private createDataToLog(error: MessageToLog): DataToLog {
    return new DataToLog(error, this.deviceInfoService.getDeviceInfo());
  }
}
