import { ILogInformationBuilder } from '../interface/log-information-builder';
import { DeviceInfo } from 'ngx-device-detector';
import { LogInformation } from 'app/logging/model/log-information.model';

export class LogInformationBuilder implements ILogInformationBuilder {
  private logInformation: LogInformation;
  private deviceInfo: DeviceInfo;

  message: string;
  stackTrace: string;
  name: string;
  cookieEnabled: boolean;
  browserSize: string;
  browser: string;
  os: string;
  url: string;
  userAgent: string;

  constructor(deviceInfo: DeviceInfo) {
    this.deviceInfo = deviceInfo;
  }

  withMessage(message: string): LogInformationBuilder {
    this.message = message;
    return this;
  }

  withStackTrace(stackTrace: string): LogInformationBuilder {
    this.stackTrace = stackTrace;
    return this;
  }

  withName(name: string): LogInformationBuilder {
    this.name = name;
    return this;
  }

  withCookieEnabled(): LogInformationBuilder {
    this.cookieEnabled = window.navigator.cookieEnabled;
    return this;
  }

  withBrowserSize(): LogInformationBuilder {
    this.browserSize = window.outerWidth + ' X ' + window.outerHeight;
    return this;
  }

  withBrowser(): LogInformationBuilder {
    this.browser = this.deviceInfo.browser + ' ' + this.deviceInfo.browser_version;
    return this;
  }

  withOs(): LogInformationBuilder {
    this.os = this.deviceInfo.os + ' ' + this.deviceInfo.os_version;
    return this;
  }

  withUserAgent(): LogInformationBuilder {
    this.userAgent = this.deviceInfo.userAgent;
    return this;
  }

  withUrl(): LogInformationBuilder {
    this.url = window.location.href;
    return this;
  }

  build(): LogInformation {
    this.logInformation = new LogInformation(this);
    return this.logInformation;
  }
}
