import { LogInformation } from 'app/logging/model/log-information.model';

export interface ILogInformationBuilder {
  /**
   *  set parameter message for Builder
   * @param message
   */
  withMessage(message: string): ILogInformationBuilder;

  /**
   *  set parameter stackTrace for Builder
   * @param stackTrace
   */
  withStackTrace(stackTrace: string): ILogInformationBuilder;

  /**
   *  set parameter cookieEnabled for Builder
   */
  withCookieEnabled(): ILogInformationBuilder;

  /**
   *  set parameter Url for Builder
   */
  withUrl(): ILogInformationBuilder;

  /**
   *  set parameter browserSize for Builder
   */
  withBrowserSize(): ILogInformationBuilder;

  /**
   *  set parameter browser for Builder
   */
  withBrowser(): ILogInformationBuilder;

  /**
   *  set parameter os for Builder
   */
  withOs(): ILogInformationBuilder;

  /**
   *  set parameter userAgent for Builder
   */
  withUserAgent(): ILogInformationBuilder;

  /**
   *  Build Object LogInformation
   */
  build(): LogInformation;
}
