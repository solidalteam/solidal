import { DataToLog } from 'app/logging/model/data-to-log.model';
import { SERVER_API_URL } from 'app/app.constants';
import { MessageToLog } from '../model/message-to-log.model';
import { Observable } from 'rxjs';

export abstract class ILog {
  public resourceUrl = SERVER_API_URL + 'api/logging';

  abstract sendInformation(dataEntry: DataToLog): Observable<any>;

  getMessage(messageToLog: MessageToLog): string {
    return messageToLog.message ? messageToLog.message : messageToLog.toString();
  }
}
