import { LogInformationBuilder } from 'app/logging/builder/LogInformationBuilder';

export class LogInformation {
  public message?: string;
  public stackTrace?: string;
  public name?: string;
  public cookieEnabled?: boolean;
  public browserSize?: string;
  public browser?: string;
  public os?: string;
  public userAgent?: string;
  public url?: string;

  constructor(info: LogInformationBuilder) {
    if (info) {
      this.message = info.message;
      this.stackTrace = info.stackTrace;
      this.cookieEnabled = info.cookieEnabled;
      this.browserSize = info.browserSize;
      this.browser = info.browser;
      this.os = info.os;
      this.userAgent = info.userAgent;
      this.url = info.url;
      this.name = info.name;
    }
  }
}
