export class MessageToLog implements Error {
  constructor(public message: string, public stack: string, public name: string) {}
}
