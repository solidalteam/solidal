import { DeviceInfo } from 'ngx-device-detector';
import { MessageToLog } from './message-to-log.model';

export class DataToLog {
  constructor(public messageToLog: MessageToLog, public deviceInfo: DeviceInfo) {}
}
