import { ILog } from '../interface/log';
import { HttpClient } from '@angular/common/http';
import { LogInformationBuilder } from '../builder/LogInformationBuilder';
import { DataToLog } from 'app/logging/model/data-to-log.model';
import { Observable } from 'rxjs';

export class LogDebug extends ILog {
  constructor(private http: HttpClient) {
    super();
  }

  sendInformation(dataEntry: DataToLog): Observable<any> {
    const logInformation = new LogInformationBuilder(dataEntry.deviceInfo)
      .withMessage(super.getMessage(dataEntry.messageToLog))
      .withStackTrace(dataEntry.messageToLog.stack)
      .withName(dataEntry.messageToLog.name)
      .withOs()
      .withCookieEnabled()
      .build();
    return this.http.post<any>(this.resourceUrl + '/debugs', logInformation);
  }
}
