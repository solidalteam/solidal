import { HttpClient } from '@angular/common/http';
import { DataToLog } from 'app/logging/model/data-to-log.model';
import { LogInformationBuilder } from 'app/logging/builder/LogInformationBuilder';
import { ILog } from 'app/logging/interface/log';
import { Observable } from 'rxjs';

export class LogInfo extends ILog {
  constructor(private http: HttpClient) {
    super();
  }

  sendInformation(dataEntry: DataToLog): Observable<any> {
    const logInformation = new LogInformationBuilder(dataEntry.deviceInfo)
      .withMessage(super.getMessage(dataEntry.messageToLog))
      .withUserAgent()
      .build();
    return this.http.post<any>(this.resourceUrl + '/infos', logInformation);
  }
}
