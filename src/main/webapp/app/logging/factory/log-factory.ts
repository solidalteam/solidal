import { ILog } from '../interface/log';
import { Injectable } from '@angular/core';
import { LogTrace } from './log-trace';
import { HttpClient } from '@angular/common/http';
import { LogInfo } from './log-info';
import { LogError } from './log-error';
import { LogDebug } from './log-debug';
import { LogWarn } from './log-warn';
import { LogType } from 'app/logging/service/client-logging.service';

@Injectable({
  providedIn: 'root'
})
export class LogFactory {
  private factory: Map<LogType, ILog>;

  constructor(private http: HttpClient) {
    this.factory = new Map<LogType, ILog>();

    this.factory.set(LogType.TRACE, new LogTrace(http));
    this.factory.set(LogType.INFO, new LogInfo(http));
    this.factory.set(LogType.ERROR, new LogError(http));
    this.factory.set(LogType.DEBUG, new LogDebug(http));
    this.factory.set(LogType.WARN, new LogWarn(http));
  }

  getLog(type: LogType): ILog {
    return this.factory.get(type);
  }
}
