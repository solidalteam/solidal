import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { JhiLanguageService } from 'ng-jhipster';

import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { LANGUAGES } from 'app/core/language/language.constants';

@Component({
  selector: 'jhi-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
  account!: Account;
  success = false;
  languages = LANGUAGES;
  settingsForm = this.fb.group({
    firstName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    lastName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    email: [undefined, [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    langKey: [undefined]
  });
  companyForm = this.fb.group({
    companyName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    vatNumber: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    creationDate: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    address1: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    address2: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    telephoneNumber: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    ownerName: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    country: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    postalCode: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    province: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]]
  });
  hasCompany = false;

  constructor(private accountService: AccountService, private fb: FormBuilder, private languageService: JhiLanguageService) {}

  ngOnInit(): void {
    this.accountService.identity().subscribe(account => {
      if (account) {
        // Consider to use Resolver
        this.hasCompany = account.authorities.includes('ROLE_COMPANY');

        if (this.hasCompany) {
          this.settingsForm.addControl('companies', this.companyForm);

          this.settingsForm.patchValue({
            firstName: account.firstName,
            lastName: account.lastName,
            email: account.email,
            langKey: account.langKey,
            companies: {
              companyName: account?.userCompanies ? account?.userCompanies[0].name : null,
              vatNumber: account?.userCompanies ? account?.userCompanies[0].vatNumber : null,
              creationDate: account?.userCompanies ? account?.userCompanies[0].creationDate : null,
              address1: account?.userCompanies ? account?.userCompanies[0].address1 : null,
              address2: account?.userCompanies ? account?.userCompanies[0].address2 : null,
              telephoneNumber: account?.userCompanies ? account?.userCompanies[0].telephoneNumber : null,
              ownerName: account?.userCompanies ? account?.userCompanies[0].ownerName : null,
              country: account?.userCompanies ? account?.userCompanies[0].country : null,
              postalCode: account?.userCompanies ? account?.userCompanies[0].postalCode : null,
              province: account?.userCompanies ? account?.userCompanies[0].province : null
            }
          });
        }

        this.settingsForm.patchValue({
          firstName: account.firstName,
          lastName: account.lastName,
          email: account.email,
          langKey: account.langKey
        });

        this.account = account;
      }
    });
  }

  save(): void {
    this.success = false;

    this.account.firstName = this.settingsForm.get('firstName')!.value;
    this.account.lastName = this.settingsForm.get('lastName')!.value;
    this.account.email = this.settingsForm.get('email')!.value;
    this.account.langKey = this.settingsForm.get('langKey')!.value;

    if (this.hasCompany) {
      this.account.userCompanies = [this.settingsForm.get('company')?.value];

      this.accountService.saveWithCompany(this.account).subscribe(() => {
        this.managePostSave();
      });
    } else {
      this.accountService.save(this.account).subscribe(() => {
        this.managePostSave();
      });
    }
  }

  private managePostSave(): void {
    this.success = true;

    this.accountService.authenticate(this.account);

    if (this.account.langKey !== this.languageService.getCurrentLanguage()) {
      this.languageService.changeLanguage(this.account.langKey);
    }
  }
}
