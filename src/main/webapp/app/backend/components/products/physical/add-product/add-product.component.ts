import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'jhi-add-product',
  templateUrl: './add-product.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  public productForm: FormGroup;
  public counter = 1;
  public url = [
    {
      img: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg'
    },
    {
      img: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg'
    },
    {
      img: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg'
    },
    {
      img: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg'
    },
    {
      img: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Gull_portrait_ca_usa.jpg'
    }
  ];

  constructor(private fb: FormBuilder) {
    this.productForm = this.fb.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      price: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      code: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z]$')]],
      size: ['', Validators.required]
    });
  }

  increment(): void {
    this.counter += 1;
  }

  decrement(): void {
    this.counter -= 1;
  }

  // FileUpload
  readUrl(event: any, i: string | number): void {
    if (event.target.files.length === 0) return;
    // Image upload validation
    const mimeType = event.target.files[0].type;

    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    // Image upload
    const reader = new FileReader();

    reader.readAsDataURL(event.target.files[0]);

    reader.onload = _event => {
      this.url[i].img = reader.result?.toString();
    };
  }

  ngOnInit(): void {}
}
