import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-product-list',
  templateUrl: './product-list.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {
  public productList = [];

  constructor() {
    this.productList = [];
  }

  ngOnInit(): void {}
}
