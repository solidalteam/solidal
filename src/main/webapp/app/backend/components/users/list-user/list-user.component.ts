import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-list-user',
  templateUrl: './list-user.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {
  public userList = [];

  public settings = {
    columns: {
      avatar: {
        title: 'Avatar',
        type: 'html'
      },
      fName: {
        title: 'First Name'
      },
      lName: {
        title: 'Last Name'
      },
      email: {
        title: 'Email'
      },
      lastLogin: {
        title: 'Last Login'
      },
      role: {
        title: 'Role'
      }
    }
  };

  constructor() {
    this.userList = [];
  }

  ngOnInit(): void {}
}
