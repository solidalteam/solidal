import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'jhi-create-user',
  templateUrl: './create-user.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  public accountForm: FormGroup | undefined;
  public permissionForm: FormGroup | undefined;

  constructor(private formBuilder: FormBuilder) {
    this.createAccountForm();
    this.createPermissionForm();
  }

  createAccountForm(): void {
    this.accountForm = this.formBuilder.group({
      fname: [''],
      lname: [''],
      email: [''],
      password: [''],
      confirmPwd: ['']
    });
  }
  createPermissionForm(): void {
    this.permissionForm = this.formBuilder.group({});
  }

  ngOnInit(): void {}
}
