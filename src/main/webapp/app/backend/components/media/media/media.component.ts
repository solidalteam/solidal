import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

@Component({
  selector: 'jhi-media',
  templateUrl: './media.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {
  public media = [];

  public settings = {
    columns: {
      img: {
        title: 'Image',
        type: 'html'
      },
      fileName: {
        title: 'File Name'
      },
      url: {
        title: 'Url'
      }
    }
  };

  public config1: DropzoneConfigInterface = {
    clickable: true,
    maxFiles: 1,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };

  constructor() {
    this.media = [];
  }

  ngOnInit(): void {}
}
