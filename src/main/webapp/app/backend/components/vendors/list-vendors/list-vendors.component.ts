import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-list-vendors',
  templateUrl: './list-vendors.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./list-vendors.component.scss']
})
export class ListVendorsComponent implements OnInit {
  public vendors = [];

  public settings = {
    actions: {
      position: 'right'
    },
    columns: {
      vendor: {
        title: 'Vendor',
        type: 'html'
      },
      products: {
        title: 'Products'
      },
      storeName: {
        title: 'Store Name'
      },
      date: {
        title: 'Date'
      },
      balance: {
        title: 'Wallet Balance'
      },
      revenue: {
        title: 'Revenue'
      }
    }
  };

  constructor() {
    this.vendors = [];
  }

  ngOnInit(): void {}
}
