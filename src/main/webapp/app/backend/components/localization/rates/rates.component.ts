import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-rates',
  templateUrl: './rates.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./rates.component.scss']
})
export class RatesComponent implements OnInit {
  public rate = [];

  public settings = {
    actions: {
      position: 'right'
    },
    columns: {
      title: {
        title: 'Currency Title'
      },
      usd: {
        title: 'USD'
      },
      code: {
        title: 'Code'
      },
      rate: {
        title: 'Exchange Rate'
      }
    }
  };

  constructor() {
    this.rate = [];
  }

  ngOnInit(): void {}
}
