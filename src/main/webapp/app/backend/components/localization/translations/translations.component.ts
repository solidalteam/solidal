import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-translations',
  templateUrl: './translations.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./translations.component.scss']
})
export class TranslationsComponent implements OnInit {
  public translations = [];

  public settings = {
    actions: {
      position: 'right'
    },
    columns: {
      userKey: {
        title: 'User Key'
      },
      russian: {
        title: 'Russian'
      },
      arabic: {
        title: 'Arabic'
      },
      english: {
        title: 'English'
      }
    }
  };

  constructor() {
    this.translations = [];
  }

  ngOnInit(): void {}
}
