import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-taxes',
  templateUrl: './taxes.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./taxes.component.scss']
})
export class TaxesComponent implements OnInit {
  public taxes = [];

  public settings = {
    actions: {
      position: 'right'
    },
    columns: {
      detail: {
        title: 'Tax Detail'
      },
      taxSchedule: {
        title: 'Tax Schedule'
      },
      rate: {
        title: 'Tax Rate'
      },
      totalAmount: {
        title: 'Total Tax Amount'
      }
    }
  };

  constructor() {
    this.taxes = [];
  }

  ngOnInit(): void {}
}
