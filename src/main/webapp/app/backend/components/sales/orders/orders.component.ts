import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'jhi-orders',
  templateUrl: './orders.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  public order: { name: string }[];
  public temp = [{ name: 'ciao' }];

  @ViewChild(DatatableComponent, { static: false }) table!: DatatableComponent;
  constructor() {
    this.order = [];
  }

  updateFilter(event: any): void {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(d => {
      return d.name.includes(val) || !val;
    });

    // update the rows
    this.order = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  ngOnInit(): void {}
}
