import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-transactions',
  templateUrl: './transactions.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  public transactions = [];

  public settings = {
    actions: false,
    hideSubHeader: true,
    columns: {
      orderId: {
        title: 'Order Id',
        filter: false
      },
      transactionId: {
        title: 'Transaction Id',
        filter: false
      },
      date: {
        title: 'Date',
        filter: false
      },
      payMethod: {
        title: 'Payment Method',
        filter: false,
        type: 'html'
      },
      deliveryStatus: {
        title: 'Delivery Status',
        filter: false
      },
      amount: {
        title: 'Amount',
        filter: false
      }
    }
  };

  constructor() {
    this.transactions = [];
  }

  ngOnInit(): void {}
}
