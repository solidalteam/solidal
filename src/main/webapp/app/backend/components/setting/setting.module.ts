import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';

import { SettingRoutingModule } from './setting-routing.module';
import { ProfileComponent } from './profile/profile.component';
import { BackendSharedModule } from '../../shared/backend-shared.module';

@NgModule({
  declarations: [ProfileComponent],
  imports: [CommonModule, NgbModule, ReactiveFormsModule, SettingRoutingModule, BackendSharedModule]
})
export class SettingModule {}
