import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-profile',
  templateUrl: './profile.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
