import { NgModule } from '@angular/core';
import { BackendComponent } from './backend.component';

import { DashboardModule } from './components/dashboard/dashboard.module';
import { BackendSharedModule } from './shared/backend-shared.module';
import { ProductsModule } from './components/products/products.module';
import { SalesModule } from './components/sales/sales.module';
import { MediaModule } from './components/media/media.module';
import { VendorsModule } from './components/vendors/vendors.module';
import { UsersModule } from './components/users/users.module';
import { LocalizationModule } from './components/localization/localization.module';
import { InvoiceModule } from './components/invoice/invoice.module';
import { SettingModule } from './components/setting/setting.module';
import { ReportsModule } from './components/reports/reports.module';

@NgModule({
  declarations: [BackendComponent],
  imports: [
    DashboardModule,
    InvoiceModule,
    SettingModule,
    ReportsModule,
    BackendSharedModule,
    LocalizationModule,
    ProductsModule,
    SalesModule,
    VendorsModule,
    MediaModule,
    UsersModule
  ],
  providers: [],
  exports: [],
  bootstrap: [BackendComponent]
})
export class BackendModule {}
