import { Directive, HostListener } from '@angular/core';

import { Screenfull } from 'screenfull';
import * as screenfull from 'screenfull';

@Directive({
  selector: '[jhiToggleFullscreen]'
})
export class ToggleFullscreenDirective {
  public screenfull!: Screenfull;

  @HostListener('click') onClick(): void {
    if ((screenfull as Screenfull).isEnabled) {
      (screenfull as Screenfull).toggle();
    }
  }
}
