import { Component, ViewEncapsulation } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Menu, NavService } from '../../service/nav.service';

@Component({
  selector: 'jhi-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class SidebarComponent {
  public menuItems: Menu[] | undefined;
  public url: any;
  public fileurl: any;

  constructor(private router: Router, public navServices: NavService) {
    this.navServices.items.subscribe(menuItems => {
      this.menuItems = menuItems;
      this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          menuItems.filter(items => {
            if (items.path === event.url) {
              this.setNavActive(items);
            }

            if (!items.children) {
              return false;
            }

            items.children.filter(subItems => {
              if (subItems.path === event.url) {
                this.setNavActive(subItems);
              }

              if (!subItems.children) {
                return false;
              }

              subItems.children.filter(subSubItems => {
                if (subSubItems.path === event.url) {
                  this.setNavActive(subSubItems);
                }
              });

              return true;
            });

            return true;
          });
        }
      });
    });
  }

  // Active Nave state
  setNavActive(item: Menu): void {
    this.menuItems?.filter(menuItem => {
      if (menuItem !== item) menuItem.active = false;
      if (menuItem.children && menuItem.children.includes(item)) menuItem.active = true;
      if (menuItem.children) {
        menuItem.children.filter(submenuItems => {
          if (submenuItems.children && submenuItems.children.includes(item)) {
            menuItem.active = true;
            submenuItems.active = true;
          }
        });
      }
    });
  }

  // Click Toggle menu
  toggletNavActive(item: Menu): void {
    if (!item.active) {
      this.menuItems?.forEach(a => {
        if (this.menuItems?.includes(item)) {
          a.active = false;
        }
        if (!a.children) {
          return false;
        }

        a.children.forEach(b => {
          if (a.children?.includes(item)) {
            b.active = false;
          }
        });

        // check if it is ok adding false
        return false;
      });
    }
    item.active = !item.active;
  }

  // Fileupload
  readUrl(event: any): void {
    if (event.target.files.length === 0) return;
    // Image upload validation
    const mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
    // Image upload
    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = _event => {
      this.url = reader.result;
    };
  }
}
