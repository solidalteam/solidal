import { Component, OnInit, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { NavService } from '../../service/nav.service';

@Component({
  selector: 'jhi-header',
  templateUrl: './header.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public rightSidebar = false;
  public open = false;
  public openNav = false;
  public isOpenMobile: boolean | undefined;

  @Output() rightSidebarEvent = new EventEmitter<boolean>();

  constructor(public navServices: NavService) {}

  collapseSidebar(): void {
    this.open = !this.open;
    this.navServices.collapseSidebar = !this.navServices.collapseSidebar;
  }

  doRightSideBar(): void {
    this.rightSidebar = !this.rightSidebar;
    this.rightSidebarEvent.emit(this.rightSidebar);
  }

  openMobileNav(): void {
    this.openNav = !this.openNav;
  }

  ngOnInit(): void {}
}
