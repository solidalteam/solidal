import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-right-sidebar',
  templateUrl: './right-sidebar.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./right-sidebar.component.scss']
})
export class RightSidebarComponent implements OnInit {
  public users = [
    {
      img: 'https://image.flaticon.com/icons/svg/149/149071.svg',
      name: 'Vincent Porter',
      status: 'Online'
    },
    {
      img: 'https://image.flaticon.com/icons/svg/149/149071.svg',
      name: 'Ain Chavez',
      status: '28 minutes ago'
    },
    {
      img: 'https://image.flaticon.com/icons/svg/149/149071.svg',
      name: 'Kori Thomas',
      status: 'Online'
    },
    {
      img: 'https://image.flaticon.com/icons/svg/149/149071.svg',
      name: 'Erica Hughes',
      status: 'Online'
    },
    {
      img: 'https://image.flaticon.com/icons/svg/149/149071.svg',
      name: 'Ginger Johnston',
      status: '2 minutes ago'
    },
    {
      img: 'https://image.flaticon.com/icons/svg/149/149071.svg',
      name: 'Prasanth Anand',
      status: '2 hour ago'
    },
    {
      img: 'https://image.flaticon.com/icons/svg/149/149071.svg',
      name: 'Hileri Jecno',
      status: 'Online'
    }
  ];

  constructor() {}

  ngOnInit(): void {}
}
