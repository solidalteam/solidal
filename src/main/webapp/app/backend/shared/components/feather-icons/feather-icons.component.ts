import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import * as feather from 'feather-icons';

@Component({
  selector: 'jhi-feather-icons',
  templateUrl: './feather-icons.component.html',
  styleUrls: ['./feather-icons.component.scss']
})
export class FeatherIconsComponent implements OnInit {
  @Input() public icon: any;

  constructor() {}

  ngOnInit(): void {
    setTimeout(() => {
      feather.replace();
    });
  }
}
