import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'jhi-root',
  templateUrl: './backend.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./backend.component.scss']
})
export class BackendComponent {
  title = 'Solidal - Backend';
}
