import { NgModule } from '@angular/core';

import './vendor';
import { SolidalSharedModule } from 'app/shared/shared.module';
import { SolidalCoreModule } from 'app/core/core.module';
import { SolidalAppRoutingModule } from './app-routing.module';
import { SolidalHomeModule } from './home/home.module';
import { SolidalEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { ProductPageModule } from 'app/pages/product-page/product-page.module';
import { SearchModule } from 'app/component/search/search.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SolidalSharedModule,
    SolidalCoreModule,
    SolidalHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    SolidalEntityModule,
    SolidalAppRoutingModule,
    ProductPageModule,
    SearchModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent]
})
export class SolidalAppModule {}
