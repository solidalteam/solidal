import { Component, OnInit, Input } from '@angular/core';
import { HomeSlider } from 'app/home/slider/slider';

@Component({
  selector: 'jhi-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {
  @Input() sliders: any[] | undefined;
  @Input() class: string | undefined;
  @Input() textClass: string | undefined;
  @Input() category: string | undefined;
  @Input() buttonText: string | undefined;
  @Input() buttonClass: string | undefined;

  public HomeSliderConfig: any = HomeSlider;

  constructor() {}

  ngOnInit(): void {}
}
