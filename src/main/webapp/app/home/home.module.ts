import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SolidalSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { CarouselComponent } from 'app/home/carousel/carousel.component';
import { SliderComponent } from 'app/home/slider/slider.component';
import { ServicesComponent } from 'app/widgets/services/services.component';

@NgModule({
  imports: [SolidalSharedModule, RouterModule.forChild([HOME_ROUTE])],
  exports: [],
  declarations: [HomeComponent, CarouselComponent, SliderComponent, ServicesComponent]
})
export class SolidalHomeModule {}
