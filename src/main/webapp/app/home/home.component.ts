import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { CompanyService } from 'app/entities/company/company.service';
import { map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ICompany } from 'app/shared/model/company.model';
import { ProductService } from 'app/entities/product/product.service';
import { ProductSlider } from 'app/home/slider/slider';
import { IProduct } from 'app/shared/model/product.model';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription: Subscription | null = null;
  products: IProduct[] = [];
  queryBasedOnCoordinatesSubscription: Subscription | undefined;
  coordinates?: Coordinates;
  productSliderConfig: any = ProductSlider;

  sliders:
    | {
        companyId: number | undefined;
        title: string | undefined;
        subTitle: string | undefined;
        image: string | undefined;
      }[]
    | undefined;

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private companyService: CompanyService,
    private productService: ProductService
  ) {
    this.sliders = [];
  }

  ngOnInit(): void {
    navigator.geolocation.getCurrentPosition((position: Position) => {
      const { coords } = position;

      this.coordinates = coords;

      const { latitude, longitude } = coords;

      this.queryBasedOnCoordinatesSubscription = this.companyService
        .queryBasedOnCoordinates(latitude, longitude)
        .pipe(map((http: HttpResponse<ICompany[]>) => http.body))
        .subscribe(companiesFromServer => {
          companiesFromServer?.forEach(company => {
            this.sliders?.push({
              companyId: company.id,
              subTitle: company.ownerName,
              title: company.name,
              image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Vanamo_Logo.png/600px-Vanamo_Logo.png'
            });
          });

          this.productService
            .query({
              sort: ['id,asc'],
              'companyId.in': companiesFromServer?.map(companies => companies.id)
            })
            .pipe(map((value: HttpResponse<IProduct[]>) => value.body))
            .subscribe(value => (this.products = value!));
        });
    });
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
    if (this.queryBasedOnCoordinatesSubscription) {
      this.queryBasedOnCoordinatesSubscription.unsubscribe();
    }
  }
}
