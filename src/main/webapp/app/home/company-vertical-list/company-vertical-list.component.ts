import { Component, Input } from '@angular/core';
import { CompanyService } from 'app/entities/company/company.service';
import { ICompany } from 'app/shared/model/company.model';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-company-vertical-list',
  templateUrl: './company-vertical-list.component.html',
  styleUrls: ['./company-vertical-list.component.scss']
})
export class CompanyVerticalListComponent {
  @Input() companies: ICompany[] | undefined;
  images = [62, 83, 466, 965, 982, 1043, 738, 50, 45, 789, 87, 65, 6, 821, 25, 52, 156, 564, 32].map(
    n => `https://picsum.photos/id/${n}/900/500`
  );

  constructor(private companyService: CompanyService, private router: Router) {}

  async redirectToSpecificCompany(companyId: number): Promise<any> {
    await this.router.navigate(['company/' + companyId + '/view']);
  }
}
