import { Component, OnInit, Input } from '@angular/core';
import { CollectionSlider } from 'app/shared/data/slider';

@Component({
  selector: 'jhi-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {
  @Input() categories: any[] | undefined;
  @Input() category: string | undefined;
  @Input() class: string | undefined;

  public collectionSliderConfig: any = CollectionSlider;

  constructor() {}

  ngOnInit(): void {}
}
