import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IProduct } from 'app/shared/model/product.model';

@Component({
  selector: 'jhi-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
  @Input() products: IProduct[] = [];
  @Input() paginate: any = {};
  @Input() layoutView = 'grid-view';
  @Input() sortBy: string | undefined;

  @Output() setGrid: EventEmitter<any> = new EventEmitter<any>();
  @Output() setLayout: EventEmitter<any> = new EventEmitter<any>();
  @Output() sortedBy: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  setGridLayoutSize(value: string): void {
    this.setGrid.emit(value);
  }

  setLayoutView(value: string): void {
    this.layoutView = value;
    this.setLayout.emit(value);
  }

  sorting(event: any): void {
    this.sortedBy.emit(event.target.value);
  }
}
