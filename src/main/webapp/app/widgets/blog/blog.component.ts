import { Component, Input, OnInit } from '@angular/core';
import { BlogSlider } from 'app/shared/data/slider';

@Component({
  selector: 'jhi-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  @Input() blogs: any[] = [];

  public blogSliderConfig: any = BlogSlider;

  constructor() {}

  ngOnInit(): void {}
}
