import { Component, OnInit, Input } from '@angular/core';
import { LogoSlider } from 'app/shared/data/slider';

@Component({
  selector: 'jhi-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {
  @Input() logos: any[] = [];

  public logoSliderConfig: any = LogoSlider;

  constructor() {}

  ngOnInit(): void {}
}
