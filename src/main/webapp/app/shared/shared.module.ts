import { NgModule } from '@angular/core';
import { SolidalSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { ProductBoxOneComponent } from 'app/component/product-box-one/product-box-one.component';
import { SkeletonProductBoxComponent } from 'app/shared/skeleton/skeleton-product-box/skeleton-product-box.component';
import { FeatherIconsComponent } from 'app/backend/shared/components/feather-icons/feather-icons.component';

@NgModule({
  imports: [SolidalSharedLibsModule],
  declarations: [
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    ProductBoxOneComponent,
    SkeletonProductBoxComponent,
    FeatherIconsComponent
  ],
  entryComponents: [LoginModalComponent],
  exports: [
    SolidalSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    ProductBoxOneComponent,
    SkeletonProductBoxComponent,
    FeatherIconsComponent
  ]
})
export class SolidalSharedModule {}
