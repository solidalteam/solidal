import { Image } from 'app/shared/model/image.model';

export interface IProduct {
  id?: number;
  productName?: string;
  productCode?: number;
  price?: number;
  description?: string;
  type?: string;
  brand?: string;
  collection?: any[];
  category?: string;
  sale?: boolean;
  discount?: number;
  stock?: number;
  isNew?: boolean;
  quantity?: number;
  tags?: any[];
  currency?: string;
  images?: Image[];
  companyId?: number;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public productName?: string,
    public productCode?: number,
    public price?: number,
    public description?: string,
    public type?: string,
    public brand?: string,
    public collection?: any[],
    public category?: string,
    public sale?: boolean,
    public discount?: number,
    public stock?: number,
    public isNew?: boolean,
    public quantity?: number,
    public tags?: any[],
    public currency?: string,
    public images?: Image[],
    public companyId?: number
  ) {}
}
