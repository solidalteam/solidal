export interface IImage {
  image_id?: number;
  id?: number;
  alt?: string;
  src?: string;
  variant_id?: any[];
}

export class Image implements IImage {
  constructor(public id?: number, public image_id?: number, public alt?: string, public src?: string) {}
}
