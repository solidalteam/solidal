import { Moment } from 'moment';
import { IProduct } from 'app/shared/model/product.model';

export interface ICompany {
  id?: number;
  name?: string;
  vatNumber?: string;
  creationDate?: Moment;
  address1?: string;
  address2?: string;
  telephoneNumber?: string;
  ownerName?: string;
  country?: string;
  postalCode?: string;
  province?: string;
  userLogin?: string;
  userId?: number;
  products?: IProduct[];
}

export class Company implements ICompany {
  constructor(
    public id?: number,
    public name?: string,
    public vatNumber?: string,
    public creationDate?: Moment,
    public address1?: string,
    public address2?: string,
    public telephoneNumber?: string,
    public ownerName?: string,
    public country?: string,
    public postalCode?: string,
    public province?: string,
    public userLogin?: string,
    public userId?: number,
    public products?: IProduct[]
  ) {}
}
