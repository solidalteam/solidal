export enum Authority {
  ADMIN = 'ROLE_ADMIN',
  USER = 'ROLE_USER',
  COMPANY = 'ROLE_COMPANY'
}
