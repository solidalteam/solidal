import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'jhi-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss']
})
export class BreadcrumbComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() breadcrumb: string | undefined;

  constructor() {}

  ngOnInit(): void {}
}
