import { Component, Input, OnInit } from '@angular/core';
import { ICompany } from 'app/shared/model/company.model';

@Component({
  selector: 'jhi-company-box',
  templateUrl: './company-box.component.html',
  styleUrls: ['./company-box.component.scss']
})
export class CompanyBoxComponent implements OnInit {
  @Input() company: ICompany | undefined;
  @Input() currency: any = 'EUR';
  @Input() thumbnail = false;
  @Input() onHoverChangeImage = false;
  @Input() cartModal = false;
  @Input() loader = false;

  imageSrc: string | undefined;

  constructor() {}

  ngOnInit(): void {
    this.loadSkeleton();
  }

  private loadSkeleton(): void {
    if (this.loader) {
      setTimeout(() => {
        this.loader = false;
      }, 2000);
    }
  }
}
