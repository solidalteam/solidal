import { Component, Input, OnInit } from '@angular/core';
import { IProduct } from 'app/shared/model/product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-product-box-one',
  templateUrl: './product-box-one.component.html',
  styleUrls: ['./product-box-one.component.scss']
})
export class ProductBoxOneComponent implements OnInit {
  @Input() product!: IProduct;
  @Input() currency: any = 'EUR';
  @Input() thumbnail = false;
  @Input() onHoverChangeImage = false;
  @Input() cartModal = false;
  @Input() loader = false;

  imageSrc: string | undefined;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.loadSkeleton();
  }

  private loadSkeleton(): void {
    if (this.loader) {
      setTimeout(() => {
        this.loader = false;
      }, 2000);
    }
  }

  async navigateToProductDetail(id: number | undefined): Promise<any> {
    await this.router.navigate(['/product', id, 'view']);
  }
}
