import { NgModule } from '@angular/core';

import { SearchResultContainerRoutingModule } from './search-result-container-routing.module';
import { SearchResultContainerComponent } from './search-result-container.component';
import { SolidalSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [SearchResultContainerComponent],
  imports: [SolidalSharedModule, SearchResultContainerRoutingModule]
})
export class SearchResultContainerModule {}
