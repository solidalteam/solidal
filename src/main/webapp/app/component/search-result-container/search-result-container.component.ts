import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jhi-search-result-container',
  templateUrl: './search-result-container.component.html',
  styleUrls: ['./search-result-container.component.scss']
})
export class SearchResultContainerComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
