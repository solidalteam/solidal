import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ProductService } from 'app/entities/product/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'jhi-search',
  templateUrl: './search.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  searchForm: FormGroup;
  isOpenMobile: boolean | undefined;

  constructor(private formBuilder: FormBuilder, private productService: ProductService, private router: Router) {
    this.searchForm = this.formBuilder.group({
      search: ['']
    });
  }

  ngOnInit(): void {}

  async searchProducts(): Promise<any> {
    this.isOpenMobile = !this.isOpenMobile;

    const search = this.searchForm.get('search');

    await this.router.navigate(['/product-page'], {
      queryParams: {
        productName: search !== null ? search.value : null
      },
      skipLocationChange: true
    });
  }
}
