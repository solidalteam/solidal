import { NgModule } from '@angular/core';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { SolidalSharedModule } from 'app/shared/shared.module';

@NgModule({
  declarations: [SearchComponent],
  exports: [SearchComponent],
  imports: [SolidalSharedModule, SearchRoutingModule]
})
export class SearchModule {}
