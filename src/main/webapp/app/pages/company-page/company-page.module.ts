import { NgModule } from '@angular/core';

import { CompanyPageRoutingModule } from './company-page-routing.module';
import { SolidalSharedModule } from 'app/shared/shared.module';
import { CompaniesComponent } from 'app/pages/company-page/companies/companies.component';
import { CompanyPageComponent } from 'app/pages/company-page/company-page.component';
import { CompanyBoxComponent } from 'app/component/company-box/company-box.component';

@NgModule({
  declarations: [CompanyPageComponent, CompaniesComponent, CompanyBoxComponent],
  imports: [CompanyPageRoutingModule, SolidalSharedModule]
})
export class CompanyPageModule {}
