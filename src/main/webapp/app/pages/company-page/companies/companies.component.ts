import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { JhiParseLinks } from 'ng-jhipster';
import { ICompany } from 'app/shared/model/company.model';
import { CompanyService } from 'app/entities/company/company.service';

@Component({
  selector: 'jhi-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss']
})
export class CompaniesComponent implements OnInit {
  companies: ICompany[];
  page: number;
  itemsPerPage: number;
  predicate: any;
  reverse: any;
  links: any;
  totalItems: number | undefined;
  grid = 'col-xl-3 col-md-6';
  layoutView = 'grid-view';
  pageNo = 1;
  paginate: any = {};
  sortBy: string | undefined;

  constructor(private companyService: CompanyService, private router: Router, protected parseLinks: JhiParseLinks) {
    this.predicate = 'id';
    this.companies = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.reverse = true;
    this.links = {
      last: 0
    };
  }

  ngOnInit(): void {
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  private loadAll(): void {
    this.companyService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe((res: HttpResponse<ICompany[]>) => this.paginateCompanies(res.body, res.headers));
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  trackId(index: number, item: ICompany): number | undefined {
    return item.id;
  }

  reset(): void {
    this.page = 0;
    this.companies = [];
    this.loadAll();
  }

  async redirectToSpecificCompany(companyId: number): Promise<any> {
    await this.router.navigate(['/company/' + companyId + '/view']);
  }

  protected paginateCompanies(data: ICompany[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') as string);
    this.totalItems = parseInt(headers.get('X-Total-Count') as string, 10);

    if (data !== null) {
      for (let i = 0; i < data.length; i++) {
        this.companies.push(data[i]);
      }
    }
  }
}
