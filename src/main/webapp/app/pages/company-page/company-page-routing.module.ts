import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { CompanyPageComponent } from 'app/pages/company-page/company-page.component';

const productPagesRoutes: Routes = [
  {
    path: '',
    component: CompanyPageComponent,
    data: {
      pageTitle: 'solidalApp.company.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(productPagesRoutes)],
  exports: [RouterModule]
})
export class CompanyPageRoutingModule {}
