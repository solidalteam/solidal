import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ProductPageComponent } from 'app/pages/product-page/product-page.component';

const productPagesRoutes: Routes = [
  {
    path: '',
    component: ProductPageComponent,
    data: {
      pageTitle: 'solidalApp.product.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(productPagesRoutes)],
  exports: [RouterModule]
})
export class ProductPageRoutingModule {}
