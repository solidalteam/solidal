import { NgModule } from '@angular/core';
import { ProductPageComponent } from 'app/pages/product-page/product-page.component';
import { SolidalSharedModule } from 'app/shared/shared.module';
import { ProductsComponent } from 'app/pages/product-page/products/products.component';
import { ProductPageRoutingModule } from 'app/pages/product-page/product-page-routing.module';

@NgModule({
  imports: [SolidalSharedModule, ProductPageRoutingModule],
  declarations: [ProductPageComponent, ProductsComponent]
})
export class ProductPageModule {}
