import { Component, Input, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ProductService } from 'app/entities/product/product.service';
import { IProduct } from 'app/shared/model/product.model';
import { Router } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { JhiParseLinks } from 'ng-jhipster';

@Component({
  selector: 'jhi-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  @Input() productName: string | undefined;

  products: IProduct[] = [];
  page: any;
  itemsPerPage: number;
  predicate: any;
  reverse: any;
  links: any;
  totalItems: number | undefined;
  grid = 'col-xl-3 col-md-6';
  layoutView = 'grid-view';
  pageNo = 1;
  paginate: any = {}; // Pagination use only
  sortBy: string | undefined; // Sorting Order

  constructor(private productService: ProductService, private router: Router, protected parseLinks: JhiParseLinks) {
    this.predicate = 'id';
    this.products = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.reverse = true;
    this.links = {
      last: 0
    };
  }

  ngOnInit(): void {
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  private loadAll(): void {
    const req = {
      page: this.page,
      size: this.itemsPerPage,
      sort: this.sort(),
      'productName.contains': this.productName ? this.productName : ''
    };

    this.productService.query(req).subscribe((res: HttpResponse<IProduct[]>) => this.paginateProducts(res.body, res.headers));
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];

    if (this.predicate !== 'id') {
      result.push('id');
    }

    return result;
  }

  trackId(index: number, item: IProduct): number | undefined {
    return item.id;
  }

  reset(): void {
    this.page = 0;
    this.products = [];
    this.loadAll();
  }

  async redirectToSpecificProduct(productId: number): Promise<any> {
    await this.router.navigate(['/product/' + productId + '/view']);
  }

  protected paginateProducts(data: IProduct[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') as string);
    this.totalItems = parseInt(headers.get('X-Total-Count') as string, 10);

    if (data !== null)
      for (let i = 0; i < data.length; i++) {
        this.products.push(data[i]);
      }
  }
}
