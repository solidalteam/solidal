package it.albertodaleo.app.component;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.AddressComponent;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

/**
 * Component for executing complex computations in order to provide information regarding geo data of user.
 */
@Component
public class ReverseGeocodingComponent {

    private static final String GOOGLE_API = "AIzaSyBF14TMJuAsMDhRGD6ippneWFRXgyOjvW8";

    private final Logger log = LoggerFactory.getLogger(ReverseGeocodingComponent.class);
    private final GeoApiContext context;

    public ReverseGeocodingComponent() {
        context = new GeoApiContext.Builder()
            .apiKey(GOOGLE_API)
            .build();
    }

    public Optional<String> reverseGeocoding(Double latitude, Double longitude) throws InterruptedException, ApiException, IOException {
        LatLng location = new LatLng(latitude, longitude);
        GeocodingResult[] results = GeocodingApi.reverseGeocode(context, location).await();
        AddressComponent[] addressComponents = results[1].addressComponents;


        return Arrays.stream(addressComponents)
            .filter(addressComponent -> Arrays.stream(addressComponent.types)
                .anyMatch(addressComponentType -> addressComponentType.name().equalsIgnoreCase("administrative_area_level_2")))
            .findFirst().map(addressComponent -> addressComponent.shortName);
    }

}
