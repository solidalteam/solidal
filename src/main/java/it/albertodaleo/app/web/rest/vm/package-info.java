/**
 * View Models used by Spring MVC REST controllers.
 */
package it.albertodaleo.app.web.rest.vm;
