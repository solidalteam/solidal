package it.albertodaleo.app.domain.geographical;

import com.google.common.base.Objects;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "city")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class City implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "city_code")
    private Long cityCode;

    @NotNull
    @Column(name = "city_name", unique = true, nullable = false)
    private String cityName;

    @NotNull
    @Column(name = "province_code", nullable = false)
    private Long provinceCode;

    @Column(name = "province_name", nullable = false)
    private String provinceName;

    @Column(name = "province_abbreviation", nullable = false)
    private String provinceAbbreviation;

    @NotNull
    @Column(name = "region_code", nullable = false)
    private Long regionCode;

    @NotNull
    @Column(name = "region_name", nullable = false)
    private String regionName;

    @NotNull
    @Column(name = "geographical_allocation_code", nullable = false)
    private Long geographicalAllocationCode;

    @NotNull
    @Column(name = "geographical_allocation", nullable = false)
    private String geographicalAllocation;

    @NotNull
    @Column(length = 4, name = "cadastral_code", nullable = false)
    private String cadastralCode;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCityCode() {
        return cityCode;
    }

    public void setCityCode(Long cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Long getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(Long provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getProvinceAbbreviation() {
        return provinceAbbreviation;
    }

    public void setProvinceAbbreviation(String provinceAbbrevation) {
        this.provinceAbbreviation = provinceAbbrevation;
    }

    public Long getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(Long regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Long getGeographicalAllocationCode() {
        return geographicalAllocationCode;
    }

    public void setGeographicalAllocationCode(Long geographicalAllocationCode) {
        this.geographicalAllocationCode = geographicalAllocationCode;
    }

    public String getGeographicalAllocation() {
        return geographicalAllocation;
    }

    public void setGeographicalAllocation(String geographicalAllocation) {
        this.geographicalAllocation = geographicalAllocation;
    }

    public String getCadastralCode() {
        return cadastralCode;
    }

    public void setCadastralCode(String cadastralCode) {
        this.cadastralCode = cadastralCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return Objects.equal(cityCode, city.cityCode) &&
            Objects.equal(cityName, city.cityName) &&
            Objects.equal(provinceCode, city.provinceCode) &&
            Objects.equal(provinceName, city.provinceName) &&
            Objects.equal(provinceAbbreviation, city.provinceAbbreviation) &&
            Objects.equal(regionCode, city.regionCode) &&
            Objects.equal(regionName, city.regionName) &&
            Objects.equal(geographicalAllocationCode, city.geographicalAllocationCode) &&
            Objects.equal(geographicalAllocation, city.geographicalAllocation) &&
            Objects.equal(cadastralCode, city.cadastralCode);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cityCode, cityName, provinceCode, provinceName, provinceAbbreviation, regionCode, regionName, geographicalAllocationCode, geographicalAllocation, cadastralCode);
    }

    @Override
    public String toString() {
        return "City{" +
            "cityCode=" + cityCode +
            ", cityName='" + cityName + '\'' +
            ", provinceCode=" + provinceCode +
            ", provinceName='" + provinceName + '\'' +
            ", provinceAbbrevation='" + provinceAbbreviation + '\'' +
            ", regionCode=" + regionCode +
            ", regionName='" + regionName + '\'' +
            ", geographicalAllocationCode=" + geographicalAllocationCode +
            ", geographicalAllocation='" + geographicalAllocation + '\'' +
            ", cadastralCode=" + cadastralCode +
            '}';
    }
}
