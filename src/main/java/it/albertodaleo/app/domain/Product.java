package it.albertodaleo.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import it.albertodaleo.app.domain.enumeration.Currency;

import it.albertodaleo.app.domain.enumeration.Type;

import it.albertodaleo.app.domain.enumeration.Category;

import it.albertodaleo.app.domain.enumeration.Tags;

/**
 * A Product.
 */
@Entity
@Table(name = "product")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 4)
    @Column(name = "product_name", nullable = false)
    private String productName;

    @NotNull
    @Column(name = "product_code", precision = 21, scale = 2, nullable = false, unique = true)
    private BigDecimal productCode;

    @NotNull
    @Column(name = "price", precision = 21, scale = 2, nullable = false)
    private BigDecimal price;

    @Column(name = "discount")
    private Double discount;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @Size(min = 50)
    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "stock", nullable = false)
    private Long stock;

    @Column(name = "is_new")
    private Boolean isNew;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private Type type;

    @Column(name = "brand")
    private String brand;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "category", nullable = false)
    private Category category;

    @Column(name = "sale")
    private Boolean sale;

    @Enumerated(EnumType.STRING)
    @Column(name = "tags")
    private Tags tags;

    @ManyToOne
    @JsonIgnoreProperties("products")
    private Company company;

    @OneToMany(mappedBy = "product")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Image> images = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public Product productName(String productName) {
        this.productName = productName;
        return this;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getProductCode() {
        return productCode;
    }

    public Product productCode(BigDecimal productCode) {
        this.productCode = productCode;
        return this;
    }

    public void setProductCode(BigDecimal productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Product price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public Product discount(Double discount) {
        this.discount = discount;
        return this;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Product currency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public Product description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStock() {
        return stock;
    }

    public Product stock(Long stock) {
        this.stock = stock;
        return this;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Boolean isIsNew() {
        return isNew;
    }

    public Product isNew(Boolean isNew) {
        this.isNew = isNew;
        return this;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Type getType() {
        return type;
    }

    public Product type(Type type) {
        this.type = type;
        return this;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public Product brand(String brand) {
        this.brand = brand;
        return this;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Category getCategory() {
        return category;
    }

    public Product category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean isSale() {
        return sale;
    }

    public Product sale(Boolean sale) {
        this.sale = sale;
        return this;
    }

    public void setSale(Boolean sale) {
        this.sale = sale;
    }

    public Tags getTags() {
        return tags;
    }

    public Product tags(Tags tags) {
        this.tags = tags;
        return this;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public Company getCompany() {
        return company;
    }

    public Product company(Company company) {
        this.company = company;
        return this;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Set<Image> getImages() {
        return images;
    }

    public Product images(Set<Image> images) {
        this.images = images;
        return this;
    }

    public Product addImage(Image image) {
        this.images.add(image);
        image.setProduct(this);
        return this;
    }

    public Product removeImage(Image image) {
        this.images.remove(image);
        image.setProduct(null);
        return this;
    }

    public void setImages(Set<Image> images) {
        this.images = images;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Product)) {
            return false;
        }
        return id != null && id.equals(((Product) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Product{" +
            "id=" + getId() +
            ", productName='" + getProductName() + "'" +
            ", productCode=" + getProductCode() +
            ", price=" + getPrice() +
            ", discount=" + getDiscount() +
            ", currency='" + getCurrency() + "'" +
            ", description='" + getDescription() + "'" +
            ", stock=" + getStock() +
            ", isNew='" + isIsNew() + "'" +
            ", type='" + getType() + "'" +
            ", brand='" + getBrand() + "'" +
            ", category='" + getCategory() + "'" +
            ", sale='" + isSale() + "'" +
            ", tags='" + getTags() + "'" +
            "}";
    }
}
