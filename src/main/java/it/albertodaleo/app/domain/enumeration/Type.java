package it.albertodaleo.app.domain.enumeration;

/**
 * The Type enumeration.
 */
public enum Type {
    VEGETABLE, MEAT
}
