package it.albertodaleo.app.domain.enumeration;

/**
 * The Tags enumeration.
 */
public enum Tags {
    ORGANIC, KM0
}
