package it.albertodaleo.app.domain.enumeration;

/**
 * The Category enumeration.
 */
public enum Category {
    FOOD, CRAFT
}
