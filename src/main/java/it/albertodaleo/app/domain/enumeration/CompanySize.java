package it.albertodaleo.app.domain.enumeration;

/**
 * The CompanySize enumeration.
 */
public enum CompanySize {
    SMALL, MEDIUM, LARGE, EXTRA_LARGE
}
