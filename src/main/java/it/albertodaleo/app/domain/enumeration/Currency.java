package it.albertodaleo.app.domain.enumeration;

/**
 * The Currency enumeration.
 */
public enum Currency {
    EUR, USD, GBP, CHF
}
