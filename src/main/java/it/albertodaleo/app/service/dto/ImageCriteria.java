package it.albertodaleo.app.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link it.albertodaleo.app.domain.Image} entity. This class is used
 * in {@link it.albertodaleo.app.web.rest.ImageResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /images?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ImageCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter imageId;

    private StringFilter alt;

    private StringFilter src;

    private StringFilter description;

    private LongFilter productId;

    public ImageCriteria() {
    }

    public ImageCriteria(ImageCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.imageId = other.imageId == null ? null : other.imageId.copy();
        this.alt = other.alt == null ? null : other.alt.copy();
        this.src = other.src == null ? null : other.src.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.productId = other.productId == null ? null : other.productId.copy();
    }

    @Override
    public ImageCriteria copy() {
        return new ImageCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getImageId() {
        return imageId;
    }

    public void setImageId(LongFilter imageId) {
        this.imageId = imageId;
    }

    public StringFilter getAlt() {
        return alt;
    }

    public void setAlt(StringFilter alt) {
        this.alt = alt;
    }

    public StringFilter getSrc() {
        return src;
    }

    public void setSrc(StringFilter src) {
        this.src = src;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getProductId() {
        return productId;
    }

    public void setProductId(LongFilter productId) {
        this.productId = productId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ImageCriteria that = (ImageCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(imageId, that.imageId) &&
            Objects.equals(alt, that.alt) &&
            Objects.equals(src, that.src) &&
            Objects.equals(description, that.description) &&
            Objects.equals(productId, that.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        imageId,
        alt,
        src,
        description,
        productId
        );
    }

    @Override
    public String toString() {
        return "ImageCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (imageId != null ? "imageId=" + imageId + ", " : "") +
                (alt != null ? "alt=" + alt + ", " : "") +
                (src != null ? "src=" + src + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (productId != null ? "productId=" + productId + ", " : "") +
            "}";
    }

}
