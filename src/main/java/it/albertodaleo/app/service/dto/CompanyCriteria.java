package it.albertodaleo.app.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link it.albertodaleo.app.domain.Company} entity. This class is used
 * in {@link it.albertodaleo.app.web.rest.CompanyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /companies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CompanyCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter vatNumber;

    private LocalDateFilter creationDate;

    private StringFilter address1;

    private StringFilter address2;

    private StringFilter telephoneNumber;

    private StringFilter ownerName;

    private StringFilter country;

    private StringFilter postalCode;

    private StringFilter province;

    private LongFilter userId;

    private LongFilter productId;

    public CompanyCriteria() {
    }

    public CompanyCriteria(CompanyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.vatNumber = other.vatNumber == null ? null : other.vatNumber.copy();
        this.creationDate = other.creationDate == null ? null : other.creationDate.copy();
        this.address1 = other.address1 == null ? null : other.address1.copy();
        this.address2 = other.address2 == null ? null : other.address2.copy();
        this.telephoneNumber = other.telephoneNumber == null ? null : other.telephoneNumber.copy();
        this.ownerName = other.ownerName == null ? null : other.ownerName.copy();
        this.country = other.country == null ? null : other.country.copy();
        this.postalCode = other.postalCode == null ? null : other.postalCode.copy();
        this.province = other.province == null ? null : other.province.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.productId = other.productId == null ? null : other.productId.copy();
    }

    @Override
    public CompanyCriteria copy() {
        return new CompanyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(StringFilter vatNumber) {
        this.vatNumber = vatNumber;
    }

    public LocalDateFilter getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateFilter creationDate) {
        this.creationDate = creationDate;
    }

    public StringFilter getAddress1() {
        return address1;
    }

    public void setAddress1(StringFilter address1) {
        this.address1 = address1;
    }

    public StringFilter getAddress2() {
        return address2;
    }

    public void setAddress2(StringFilter address2) {
        this.address2 = address2;
    }

    public StringFilter getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(StringFilter telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public StringFilter getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(StringFilter ownerName) {
        this.ownerName = ownerName;
    }

    public StringFilter getCountry() {
        return country;
    }

    public void setCountry(StringFilter country) {
        this.country = country;
    }

    public StringFilter getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(StringFilter postalCode) {
        this.postalCode = postalCode;
    }

    public StringFilter getProvince() {
        return province;
    }

    public void setProvince(StringFilter province) {
        this.province = province;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getProductId() {
        return productId;
    }

    public void setProductId(LongFilter productId) {
        this.productId = productId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CompanyCriteria that = (CompanyCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(vatNumber, that.vatNumber) &&
            Objects.equals(creationDate, that.creationDate) &&
            Objects.equals(address1, that.address1) &&
            Objects.equals(address2, that.address2) &&
            Objects.equals(telephoneNumber, that.telephoneNumber) &&
            Objects.equals(ownerName, that.ownerName) &&
            Objects.equals(country, that.country) &&
            Objects.equals(postalCode, that.postalCode) &&
            Objects.equals(province, that.province) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(productId, that.productId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        vatNumber,
        creationDate,
        address1,
        address2,
        telephoneNumber,
        ownerName,
        country,
        postalCode,
        province,
        userId,
        productId
        );
    }

    @Override
    public String toString() {
        return "CompanyCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (vatNumber != null ? "vatNumber=" + vatNumber + ", " : "") +
                (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
                (address1 != null ? "address1=" + address1 + ", " : "") +
                (address2 != null ? "address2=" + address2 + ", " : "") +
                (telephoneNumber != null ? "telephoneNumber=" + telephoneNumber + ", " : "") +
                (ownerName != null ? "ownerName=" + ownerName + ", " : "") +
                (country != null ? "country=" + country + ", " : "") +
                (postalCode != null ? "postalCode=" + postalCode + ", " : "") +
                (province != null ? "province=" + province + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (productId != null ? "productId=" + productId + ", " : "") +
            "}";
    }

}
