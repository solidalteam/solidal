package it.albertodaleo.app.service.dto;

import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.*;
import it.albertodaleo.app.domain.enumeration.Category;
import it.albertodaleo.app.domain.enumeration.Currency;
import it.albertodaleo.app.domain.enumeration.Tags;
import it.albertodaleo.app.domain.enumeration.Type;

import java.io.Serializable;
import java.util.Objects;

/**
 * Criteria class for the {@link it.albertodaleo.app.domain.Product} entity. This class is used
 * in {@link it.albertodaleo.app.web.rest.ProductResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /products?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProductCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Currency
     */
    public static class CurrencyFilter extends Filter<Currency> {

        public CurrencyFilter() {
        }

        public CurrencyFilter(CurrencyFilter filter) {
            super(filter);
        }

        @Override
        public CurrencyFilter copy() {
            return new CurrencyFilter(this);
        }

    }

    /**
     * Class for filtering Type
     */
    public static class TypeFilter extends Filter<Type> {

        public TypeFilter() {
        }

        public TypeFilter(TypeFilter filter) {
            super(filter);
        }

        @Override
        public TypeFilter copy() {
            return new TypeFilter(this);
        }

    }

    /**
     * Class for filtering Category
     */
    public static class CategoryFilter extends Filter<Category> {

        public CategoryFilter() {
        }

        public CategoryFilter(CategoryFilter filter) {
            super(filter);
        }

        @Override
        public CategoryFilter copy() {
            return new CategoryFilter(this);
        }

    }

    /**
     * Class for filtering Tags
     */
    public static class TagsFilter extends Filter<Tags> {

        public TagsFilter() {
        }

        public TagsFilter(TagsFilter filter) {
            super(filter);
        }

        @Override
        public TagsFilter copy() {
            return new TagsFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter productName;

    private BigDecimalFilter productCode;

    private BigDecimalFilter price;

    private DoubleFilter discount;

    private CurrencyFilter currency;

    private StringFilter description;

    private LongFilter stock;

    private BooleanFilter isNew;

    private TypeFilter type;

    private StringFilter brand;

    private CategoryFilter category;

    private BooleanFilter sale;

    private TagsFilter tags;

    private LongFilter companyId;

    private LongFilter imageId;

    public ProductCriteria() {
    }

    public ProductCriteria(ProductCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.productName = other.productName == null ? null : other.productName.copy();
        this.productCode = other.productCode == null ? null : other.productCode.copy();
        this.price = other.price == null ? null : other.price.copy();
        this.discount = other.discount == null ? null : other.discount.copy();
        this.currency = other.currency == null ? null : other.currency.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.stock = other.stock == null ? null : other.stock.copy();
        this.isNew = other.isNew == null ? null : other.isNew.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.brand = other.brand == null ? null : other.brand.copy();
        this.category = other.category == null ? null : other.category.copy();
        this.sale = other.sale == null ? null : other.sale.copy();
        this.tags = other.tags == null ? null : other.tags.copy();
        this.companyId = other.companyId == null ? null : other.companyId.copy();
        this.imageId = other.imageId == null ? null : other.imageId.copy();
    }

    @Override
    public ProductCriteria copy() {
        return new ProductCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getProductName() {
        return productName;
    }

    public void setProductName(StringFilter productName) {
        this.productName = productName;
    }

    public BigDecimalFilter getProductCode() {
        return productCode;
    }

    public void setProductCode(BigDecimalFilter productCode) {
        this.productCode = productCode;
    }

    public BigDecimalFilter getPrice() {
        return price;
    }

    public void setPrice(BigDecimalFilter price) {
        this.price = price;
    }

    public DoubleFilter getDiscount() {
        return discount;
    }

    public void setDiscount(DoubleFilter discount) {
        this.discount = discount;
    }

    public CurrencyFilter getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyFilter currency) {
        this.currency = currency;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public LongFilter getStock() {
        return stock;
    }

    public void setStock(LongFilter stock) {
        this.stock = stock;
    }

    public BooleanFilter getIsNew() {
        return isNew;
    }

    public void setIsNew(BooleanFilter isNew) {
        this.isNew = isNew;
    }

    public TypeFilter getType() {
        return type;
    }

    public void setType(TypeFilter type) {
        this.type = type;
    }

    public StringFilter getBrand() {
        return brand;
    }

    public void setBrand(StringFilter brand) {
        this.brand = brand;
    }

    public CategoryFilter getCategory() {
        return category;
    }

    public void setCategory(CategoryFilter category) {
        this.category = category;
    }

    public BooleanFilter getSale() {
        return sale;
    }

    public void setSale(BooleanFilter sale) {
        this.sale = sale;
    }

    public TagsFilter getTags() {
        return tags;
    }

    public void setTags(TagsFilter tags) {
        this.tags = tags;
    }

    public LongFilter getCompanyId() {
        return companyId;
    }

    public void setCompanyId(LongFilter companyId) {
        this.companyId = companyId;
    }

    public LongFilter getImageId() {
        return imageId;
    }

    public void setImageId(LongFilter imageId) {
        this.imageId = imageId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProductCriteria that = (ProductCriteria) o;
        return
            Objects.equals(id, that.id) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(productCode, that.productCode) &&
                Objects.equals(price, that.price) &&
                Objects.equals(discount, that.discount) &&
                Objects.equals(currency, that.currency) &&
                Objects.equals(description, that.description) &&
                Objects.equals(stock, that.stock) &&
                Objects.equals(isNew, that.isNew) &&
                Objects.equals(type, that.type) &&
                Objects.equals(brand, that.brand) &&
                Objects.equals(category, that.category) &&
                Objects.equals(sale, that.sale) &&
                Objects.equals(tags, that.tags) &&
                Objects.equals(companyId, that.companyId) &&
                Objects.equals(imageId, that.imageId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            productName,
            productCode,
            price,
            discount,
            currency,
            description,
            stock,
            isNew,
            type,
            brand,
            category,
            sale,
            tags,
            companyId,
            imageId
        );
    }

    @Override
    public String toString() {
        return "ProductCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (productName != null ? "productName=" + productName + ", " : "") +
            (productCode != null ? "productCode=" + productCode + ", " : "") +
            (price != null ? "price=" + price + ", " : "") +
            (discount != null ? "discount=" + discount + ", " : "") +
            (currency != null ? "currency=" + currency + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (stock != null ? "stock=" + stock + ", " : "") +
            (isNew != null ? "isNew=" + isNew + ", " : "") +
            (type != null ? "type=" + type + ", " : "") +
            (brand != null ? "brand=" + brand + ", " : "") +
            (category != null ? "category=" + category + ", " : "") +
            (sale != null ? "sale=" + sale + ", " : "") +
            (tags != null ? "tags=" + tags + ", " : "") +
            (companyId != null ? "companyId=" + companyId + ", " : "") +
            (imageId != null ? "imageId=" + imageId + ", " : "") +
            "}";
    }

}
