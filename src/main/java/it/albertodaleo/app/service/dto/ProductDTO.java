package it.albertodaleo.app.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import it.albertodaleo.app.domain.enumeration.Currency;
import it.albertodaleo.app.domain.enumeration.Type;
import it.albertodaleo.app.domain.enumeration.Category;
import it.albertodaleo.app.domain.enumeration.Tags;

/**
 * A DTO for the {@link it.albertodaleo.app.domain.Product} entity.
 */
public class ProductDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(min = 4)
    private String productName;

    @NotNull
    private BigDecimal productCode;

    @NotNull
    private BigDecimal price;

    private Double discount;

    @NotNull
    private Currency currency;

    @Size(min = 50)
    private String description;

    @NotNull
    private Long stock;

    private Boolean isNew;

    @NotNull
    private Type type;

    private String brand;

    @NotNull
    private Category category;

    private Boolean sale;

    private Tags tags;


    private Long companyId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getProductCode() {
        return productCode;
    }

    public void setProductCode(BigDecimal productCode) {
        this.productCode = productCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }

    public Boolean isIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Boolean isSale() {
        return sale;
    }

    public void setSale(Boolean sale) {
        this.sale = sale;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductDTO productDTO = (ProductDTO) o;
        if (productDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), productDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
            "id=" + getId() +
            ", productName='" + getProductName() + "'" +
            ", productCode=" + getProductCode() +
            ", price=" + getPrice() +
            ", discount=" + getDiscount() +
            ", currency='" + getCurrency() + "'" +
            ", description='" + getDescription() + "'" +
            ", stock=" + getStock() +
            ", isNew='" + isIsNew() + "'" +
            ", type='" + getType() + "'" +
            ", brand='" + getBrand() + "'" +
            ", category='" + getCategory() + "'" +
            ", sale='" + isSale() + "'" +
            ", tags='" + getTags() + "'" +
            ", companyId=" + getCompanyId() +
            "}";
    }
}
