package it.albertodaleo.app.service;

import com.google.maps.errors.ApiException;
import io.github.jhipster.service.QueryService;
import io.github.jhipster.service.filter.StringFilter;
import it.albertodaleo.app.component.ReverseGeocodingComponent;
import it.albertodaleo.app.domain.Company;
import it.albertodaleo.app.domain.Company_;
import it.albertodaleo.app.domain.Product_;
import it.albertodaleo.app.domain.User_;
import it.albertodaleo.app.exception.ProvinceNotFoundException;
import it.albertodaleo.app.repository.CompanyRepository;
import it.albertodaleo.app.repository.UserRepository;
import it.albertodaleo.app.service.dto.CompanyCriteria;
import it.albertodaleo.app.service.dto.CompanyDTO;
import it.albertodaleo.app.service.mapper.CompanyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;
import java.io.IOException;
import java.util.List;

/**
 * Service for executing complex queries for {@link Company} entities in the database.
 * The main input is a {@link CompanyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CompanyDTO} or a {@link Page} of {@link CompanyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CompanyQueryService extends QueryService<Company> {

    private final Logger log = LoggerFactory.getLogger(CompanyQueryService.class);

    private final CompanyRepository companyRepository;

    private final CompanyMapper companyMapper;

    private final UserRepository userRepository;

    private final ReverseGeocodingComponent reverseGeocodingComponent;

    public CompanyQueryService(CompanyRepository companyRepository, CompanyMapper companyMapper, UserRepository userRepository, ReverseGeocodingComponent reverseGeocodingComponent) {
        this.companyRepository = companyRepository;
        this.companyMapper = companyMapper;
        this.userRepository = userRepository;
        this.reverseGeocodingComponent = reverseGeocodingComponent;
    }

    /**
     * Return a {@link List} of {@link CompanyDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CompanyDTO> findByCriteria(CompanyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Company> specification = createSpecification(criteria);
        return companyMapper.toDto(companyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CompanyDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyDTO> findByCriteria(CompanyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Company> specification = createSpecification(criteria);
        return companyRepository.findAll(specification, page)
            .map(companyMapper::toDto);
    }

    /**
     * Return a {@link Page} of {@link CompanyDTO} which matches the criteria from the database and the user
     *
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyDTO> findByCriteriaAndCompanyUser(Pageable page) {
        log.debug("page: {}", page);

        return companyRepository.findByUserIsCurrentUser(page).map(companyMapper::toDto);
    }


    /**
     * Return a {@link Page} of {@link CompanyDTO} which matches the criteria from the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page     The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyDTO> findByCriteriaDependingOnCoordinates(Double latitude, Double longitude, CompanyCriteria criteria, Pageable page) throws InterruptedException, ApiException, IOException, IOException, ApiException {
        log.debug("find by criteria : {}, page: {}", criteria, page);

        String province = reverseGeocodingComponent.reverseGeocoding(latitude, longitude).orElseThrow(ProvinceNotFoundException::new);

        log.info("User is located in the province: {}", province);

        StringFilter provinceFilter = new StringFilter();
        provinceFilter.setContains(province);

        criteria.setProvince(provinceFilter);

        final Specification<Company> specification = createSpecification(criteria);

        return companyRepository.findAll(specification, page)
            .map(companyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CompanyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Company> specification = createSpecification(criteria);
        return companyRepository.count(specification);
    }

    /**
     * Function to convert {@link CompanyCriteria} to a {@link Specification}
     *
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Company> createSpecification(CompanyCriteria criteria) {
        Specification<Company> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Company_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Company_.name));
            }
            if (criteria.getVatNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getVatNumber(), Company_.vatNumber));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), Company_.creationDate));
            }
            if (criteria.getAddress1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress1(), Company_.address1));
            }
            if (criteria.getAddress2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress2(), Company_.address2));
            }
            if (criteria.getTelephoneNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTelephoneNumber(), Company_.telephoneNumber));
            }
            if (criteria.getOwnerName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOwnerName(), Company_.ownerName));
            }
            if (criteria.getCountry() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountry(), Company_.country));
            }
            if (criteria.getPostalCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPostalCode(), Company_.postalCode));
            }
            if (criteria.getProvince() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProvince(), Company_.province));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Company_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductId(),
                    root -> root.join(Company_.products, JoinType.LEFT).get(Product_.id)));
            }
        }
        return specification;
    }
}
