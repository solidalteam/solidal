package it.albertodaleo.app.service.mapper;


import it.albertodaleo.app.domain.*;
import it.albertodaleo.app.service.dto.ImageDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Image} and its DTO {@link ImageDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class})
public interface ImageMapper extends EntityMapper<ImageDTO, Image> {

    @Mapping(source = "product.id", target = "productId")
    ImageDTO toDto(Image image);

    @Mapping(source = "productId", target = "product")
    Image toEntity(ImageDTO imageDTO);

    default Image fromId(Long id) {
        if (id == null) {
            return null;
        }
        Image image = new Image();
        image.setId(id);
        return image;
    }
}
