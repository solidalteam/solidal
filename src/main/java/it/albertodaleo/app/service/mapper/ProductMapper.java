package it.albertodaleo.app.service.mapper;


import it.albertodaleo.app.domain.*;
import it.albertodaleo.app.service.dto.ProductDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = {CompanyMapper.class})
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

    @Mapping(source = "company.id", target = "companyId")
    ProductDTO toDto(Product product);

    @Mapping(source = "companyId", target = "company")
    @Mapping(target = "images", ignore = true)
    @Mapping(target = "removeImage", ignore = true)
    Product toEntity(ProductDTO productDTO);

    default Product fromId(Long id) {
        if (id == null) {
            return null;
        }
        Product product = new Product();
        product.setId(id);
        return product;
    }
}
