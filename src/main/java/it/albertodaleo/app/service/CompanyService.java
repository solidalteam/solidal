package it.albertodaleo.app.service;

import it.albertodaleo.app.domain.Company;
import it.albertodaleo.app.domain.User;
import it.albertodaleo.app.repository.CompanyRepository;
import it.albertodaleo.app.repository.UserRepository;
import it.albertodaleo.app.service.dto.CompanyDTO;
import it.albertodaleo.app.service.mapper.CompanyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Company}.
 */
@Service
@Transactional
public class CompanyService {

    private final Logger log = LoggerFactory.getLogger(CompanyService.class);

    private final CompanyRepository companyRepository;

    private final UserRepository userRepository;

    private final CompanyMapper companyMapper;

    public CompanyService(CompanyRepository companyRepository, UserRepository userRepository, CompanyMapper companyMapper) {
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.companyMapper = companyMapper;
    }

    /**
     * Save a company.
     *
     * @param companyDTO the entity to save.
     * @return the persisted entity.
     */
    public CompanyDTO update(CompanyDTO companyDTO, Principal principal) {
        log.debug("Request to save Company : {}", companyDTO);
        Optional<User> userActivated = getUserActivated(principal);
        if (userActivated.isPresent() && userActivated.get().getId().intValue() == companyDTO.getUserId().intValue()) {
            Company company = companyMapper.toEntity(companyDTO);
            company = companyRepository.save(company);
            return companyMapper.toDto(company);
        } else {
            throw new UsernameNotFoundException("User who make the request seem not owning the company or is not activated yet.");
        }
    }

    private Optional<User> getUserActivated(Principal principal) {
        return userRepository.findOneByLogin(principal.getName()).filter(User::getActivated);
    }

    /**
     * Save a company.
     *
     * @param companyDTO the entity to save.
     * @param principal
     * @return the persisted entity.
     */
    public CompanyDTO save(CompanyDTO companyDTO, Principal principal) {
        log.debug("Request to save Company : {}", companyDTO);
        Company company = companyMapper.toEntity(companyDTO);
        Optional<User> userFound = getUserActivated(principal);
        if (userFound.isPresent()) {
            company.user(userFound.get());
            company = companyRepository.save(company);

            return companyMapper.toDto(company);
        } else {
            throw new UsernameNotFoundException("User who make the request seem not having a proper account on database or is not activated yet.");
        }
    }

    /**
     * Get all the companies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CompanyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Companies");
        return companyRepository.findAll(pageable)
            .map(companyMapper::toDto);
    }

    /**
     * Get one company by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CompanyDTO> findOne(Long id) {
        log.debug("Request to get Company : {}", id);
        return companyRepository.findById(id)
            .map(companyMapper::toDto);
    }

    /**
     * Delete the company by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Company : {}", id);
        companyRepository.deleteById(id);
    }
}
