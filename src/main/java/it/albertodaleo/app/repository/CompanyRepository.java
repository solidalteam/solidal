package it.albertodaleo.app.repository;

import it.albertodaleo.app.domain.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Company entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompanyRepository extends JpaRepository<Company, Long>, JpaSpecificationExecutor<Company> {

    @Query("select company from Company company where company.user.login = ?#{principal.username}")
    Page<Company> findByUserIsCurrentUser(Pageable pageable);

    @Query("select company from Company company where company.user.login = ?#{principal.username}")
    List<Company> findByUserIsCurrentUser();
}
