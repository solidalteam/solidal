package it.albertodaleo.app.repository;

import it.albertodaleo.app.domain.geographical.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Company entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CityRepository extends JpaRepository<City, Long>, JpaSpecificationExecutor<City> {

    List<City> findAllByProvinceAbbreviationOrProvinceName(String provinceAbbreviation, String provinceName);
}
